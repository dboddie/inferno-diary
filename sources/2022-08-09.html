<!-- title -->Getting Sidetracked (Again)<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- content -->
<p>
As in the <a href="2022-03-03.html">previous entry</a>, I got sidetracked on
the other Inferno-related activity I was writing about:
<a href="2022-01-13.html">my personal organiser application</a>.
The problem I encountered was that exiting the application caused a trap to
occur in Inferno, probably related to some aspect of my port to the NanoNote
that I hadn't implemented correctly. Nonetheless, I wasn't encouraged to dig
into backtraces and MIPS32 machine code to trace the problem.
</p>

<h2>Not quite a fresh start</h2>

<p>
Instead, I went back and did what I sometimes do when frustrated with various
technologies: try and make something simpler and easier for me to understand.
This usually involves thinking about programming languages, compilers and
interpreters, experimenting with implementations, rediscovering all the things
that need to be done when implementing parsers and code generators, especially
all the details that I tend to forget after dealing with them the last time.
</p>

<p>
The work-in-progress result was that I implemented yet another simple
programming language based on ideas from two previous languages: one was a
variation on the usual Python-like interpreted language I often return to; the
other was an S-expression-based language that was compiled to Thumb-2 code.
I was quite happy with the interpreted language but felt that I had made
mistakes with the interpreter implementation, discarding a stack-based virtual
machine and using a register-based one instead without thinking about some of
the implications. The compiler for the compiled language was arguably better
implemented, but I just found it awkward to express things in the language
syntax.
</p>

<p>
With these limitations in mind, I took the syntax of the interpreted language
and tried to reuse as much of the compiler for the second language as I could.
The result is still a mess of sorts, but at least it's a new start and
something to work with and build upon. It also considers some of the things
that programs need when used on embedded hardware, such as allowing easy
definition of static and dynamic data areas.
</p>

<h2>Making it work</h2>

<p>
It's all very well designing languages and running tests but the real test is
how it all fits together when building something a bit more complex than a
simple demo application. Fortunately, the two language implementations I reused
had already been exercised to a certain extent, so I wasn't worried about
finding too many missing features. As to a real test, I had already decided that
<a href="2022-01-13.html">my personal organiser application</a> needed
reimplementing. This is where the relevance to Inferno comes in because it
involves rebuilding the interpreter for the NanoNote using the <tt>spim</tt>
flavour of the Inferno C compiler.
</p>

<p>
The appearance of the application is a bit more basic than the previous
version because I'm not able to use the same set of graphical features
that Inferno provides, but it can be improved in time, and the idea was to get
something working fairly quickly. The interpreted language takes care of most
of the processing, using a C function to copy image data around.
</p>

<div class="centre"><img class="dark" src="images/2022-08-09-menu.png" />
<img class="dark" src="images/2022-08-09-calendar.png" />
<img class="dark" src="images/2022-08-09-contacts.png" /><br/>
<span class="caption">The main menu, calendar and contacts pages</span></div>

<p>
Of course, there were going to be mismatches in the styles of programming used
for the Limbo-based application and the new one I was writing. In Limbo I was
relying on function pointers to implement a callback mechanism; in my language
I rely on a simple inheritance system and run-time method dispatch to give
instances of types different behaviour. On the whole, I find the code easier to
understand, and I don't find myself fighting the language quite so much. Still,
there are things I want to improve with the language itself, and writing this
application is a good way to find things to improve. I already have some
frustrations with the way parts of the language work, and I feel that some
things should be simpler.
</p>

<p>
There's not much to show in terms of code that would look very different to a
snippet of Python source code. The syntax is a bit like Python, but maybe the
method syntax is a bit Limbo-like.
</p>

<div class="highlight"><pre><span></span><span class="k">def</span> <span class="nf">MainMenuPage</span><span class="o">.</span><span class="n">keypress</span><span class="p">(</span><span class="bp">self</span><span class="p">:</span> <span class="n">MainMenuPage</span><span class="p">,</span> <span class="n">ch</span><span class="p">:</span> <span class="nb">int</span><span class="p">)</span> <span class="k">returns</span> <span class="nb">bool</span>
    <span class="k">if</span> <span class="n">ch</span> <span class="o">==</span> <span class="s1">&#39;</span><span class="se">\r</span><span class="s1">&#39;</span>
        <span class="bp">self</span><span class="o">.</span><span class="n">gui</span><span class="o">.</span><span class="n">show_page</span><span class="p">(</span><span class="bp">self</span><span class="o">.</span><span class="n">menu</span><span class="o">.</span><span class="n">selected</span> <span class="o">+</span> <span class="mi">1</span><span class="p">)</span>
        <span class="k">return</span> <span class="n">true</span>
    <span class="k">elif</span> <span class="n">ch</span> <span class="o">==</span> <span class="n">KeyEsc</span>
        <span class="bp">self</span><span class="o">.</span><span class="n">gui</span><span class="o">.</span><span class="n">running</span> <span class="o">=</span> <span class="n">false</span>
        <span class="k">return</span> <span class="n">true</span>
    <span class="k">else</span>
        <span class="c1"># Send keypresses to widgets.</span>
        <span class="k">return</span> <span class="n">Page</span><span class="o">.</span><span class="n">keypress</span><span class="p">(</span><span class="bp">self</span><span class="p">,</span> <span class="n">ch</span><span class="p">)</span>
    <span class="k">end</span>
<span class="k">end</span>
</pre></div>

<p>
Types can be derived from others and specialised methods can be created to
override those from the base type. However, when defining interfaces or
collections of related types, we usually need to specify a common base type for
compatibility. The compiler will only see that base type when generating a
method call because the actual type may only be known at run-time.
This is illustrated by the following method, where the <tt>page</tt> variable
holds an instance of the <tt>Page</tt> type or, more likely, an instance of
a <tt>Page</tt> subtype.
</p>

<div class="highlight"><pre><span></span><span class="k">def</span> <span class="nf">GUI</span><span class="o">.</span><span class="n">show_page</span><span class="p">(</span><span class="bp">self</span><span class="p">:</span> <span class="n">GUI</span><span class="p">,</span> <span class="n">index</span><span class="p">:</span> <span class="nb">int</span><span class="p">)</span>
    <span class="k">if</span> <span class="n">index</span> <span class="o">&gt;=</span> <span class="mi">0</span> <span class="ow">and</span> <span class="n">index</span> <span class="o">&lt;</span> <span class="bp">self</span><span class="o">.</span><span class="n">pages</span><span class="o">.</span><span class="n">len</span><span class="p">()</span>
        <span class="bp">self</span><span class="o">.</span><span class="n">current_page</span> <span class="o">=</span> <span class="n">index</span>
        <span class="n">page</span> <span class="o">=</span> <span class="bp">self</span><span class="o">.</span><span class="n">pages</span><span class="p">[</span><span class="n">index</span><span class="p">]</span>
        <span class="bp">self</span><span class="o">.</span><span class="n">surface</span><span class="o">.</span><span class="n">draw_rect</span><span class="p">(</span><span class="mi">0</span><span class="p">,</span> <span class="mi">0</span><span class="p">,</span> <span class="bp">self</span><span class="o">.</span><span class="n">surface</span><span class="o">.</span><span class="n">width</span><span class="p">,</span> <span class="bp">self</span><span class="o">.</span><span class="n">surface</span><span class="o">.</span><span class="n">height</span><span class="p">,</span> <span class="bp">self</span><span class="o">.</span><span class="n">bg</span><span class="p">)</span>
        <span class="n">__sub</span><span class="p">(</span><span class="n">page</span><span class="p">)</span><span class="o">.</span><span class="n">show</span><span class="p">()</span>
        <span class="bp">self</span><span class="o">.</span><span class="n">focus</span> <span class="o">=</span> <span class="n">page</span>
    <span class="k">end</span>
<span class="k">end</span>
</pre></div>

<p>
The <tt>__sub</tt> function is a special function that tells to compiler to
generate code to resolve the type at run-time and call the appropriate method.
All of this extra processing could be avoided if the actual type of the page
was used in this method, but that would imply that we would know the type at
compile-time, and <tt>page</tt> can be an instance of any number of <tt>Page</tt>
subtypes.
</p>

<p>
This resolution of types and the use of dynamic dispatch in addition to static
dispatch is something I'm still trying to resolve in my plans for the language.
On the one hand, specialising types at compile-time makes it possible to
generate efficient code. On the other hand, some features require type
resolution to occur at run-time, making aspects of static dispatch redundant.
</p>
<!-- /content -->

<!-- name -->David Boddie<!-- /name -->
<!-- date -->9 August 2022<!-- /date -->
