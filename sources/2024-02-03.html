<!-- title -->Playing with Interpreters<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- summary -->
Adding a simple interpreter to Inferno.
<!-- /summary -->

<!-- content -->
<p>
When porting Inferno to different microcontrollers there was a parallel
activity that helped me to get an understanding of how the hardware behaved,
and which allowed me to test different mechanisms and peripherals before trying
to attempt a port. This was the creation of some bare metal code examples,
which I mostly wrote in a simple language that I wrote a compiler and an
interpreter for. You can see a bit of the language syntax in
<a href="2022-08-09.html">this earlier diary entry</a>.
</p>

<p>
The compiler mostly targets processors with the Thumb-2 instruction set. The
interpreter is written in C, and it builds with either GCC or the various C
compilers that Inferno provides. This allowed me to run my code on the Ben
NanoNote without having written any MIPS code.
</p>

<p>
Recently, when experimenting with some Limbo code, I decided that I'd prefer
to write programs for Inferno in another language, so I started thinking about
making my interpreter run under Inferno. Since Inferno isn't really designed to
allow arbitrary C programs to run, there needs to be a way to invoke the
interpreter, and this is where investigations begin.
</p>

<h2>Three approaches</h2>

<p>
There seemed to be three ways to integrate the interpreter into Inferno:
</p>

<ol>
<li>As a file system: send a file name or program code to a <tt>ctl</tt> file
and the interpreter runs it.</li>
<li>Modify the module loader in <tt>libinterp/xec.c</tt> to do something
different when supplied with a file in a non-Dis runnable format.</li>
<li>Create a Limbo module to invoke the interpreter.</li>
</ol>

<p>
This was also the order in which I attempted the integration.
</p>

<h2>Intepreter file system</h2>

<p>
It might seem odd to invoke an intepreter via a file system interface, running
a script like this:
</p>

<pre>
echo 'file.bin' > /dev/interpreter/ctl
</pre>

<p>
The problem lies with the way programs are run under Inferno. Since the system
was designed around a single virtual machine, all programs are expected to be
written as Limbo modules and compiled to Dis virtual machine code.
The mechanism to run programs involves loading each program as a module and
hooking it up to the virtual machine. There is no mechanism for dispatching a
file in an alternative binary format to another virtual machine.
</p>

<p>
By using a file system, we sidestep this problem. However, experiments proved
problematic when trying to write to the standard output file descriptor.
If a new interpreter process is started with the descriptors from the parent
process, writing to standard output causes the process to write more input to
the file system, which is not what we want.
</p>

<p>
In the end, this seemed like a nice trick, but didn't really work.
</p>

<h2>Modified loader</h2>

<p>
Initially, I wanted to do something similar to my <a href="2023-11-18.html">frozen
module work</a>, adding code to check the start of an executable file and
sneaking it away from Dis to run it with my interpreter. However, by the time
a file has reached this point in the virtual machine's loader, it's pretty much
assumed to be Dis code, and there's no clean way to back out of the process of
processing it in normal way without causing an error.
</p>

<p>
I tried to perform checks in the <tt>iload</tt> operation in <tt>libinterp/xec.c</tt>
instead, but it's still part of the processing pipeline for Dis code, and led
only to disappointment.
</p>

<h2>Limbo module</h2>

<p>
I was aware that there was an introduction to
<a href="https://powerman.name/doc/Inferno/c_module_en">Developing Limbo
modules in C</a>, and it seemed like taking this approach, even in a minimal
way, would allow the interpreter to be integrated in a clean way while also
providing the benefit that interpreter processes would be visible to the
<tt>ps</tt> command.
</p>

<p>
The interface we provide to the interpreter doesn't need all the features
covered in the guide. It only needs to provide a command to run a file, as the
module file illustrates:
</p>

<div class="highlight"><pre><span></span><span class="n">Winter</span><span class="p">:</span> <span class="n">module</span>
<span class="p">{</span>
    <span class="n">PATH</span><span class="p">:</span> <span class="n">con</span> <span class="s">&quot;$Winter&quot;</span><span class="p">;</span>

    <span class="nb">run</span><span class="p">:</span> <span class="n">fn</span><span class="p">(</span><span class="nb">path</span><span class="p">:</span> <span class="n">string</span><span class="p">);</span>
<span class="p">};</span>
</pre></div>

<p>
A wrapper module is used to invoke scripts:
</p>

<div class="highlight"><pre><span></span><span class="k">implement</span> <span class="n">WinterRun</span><span class="p">;</span>

<span class="k">include</span> <span class="s">&quot;draw.m&quot;</span><span class="p">;</span>
<span class="k">include</span> <span class="s">&quot;sys.m&quot;</span><span class="p">;</span>
<span class="k">include</span> <span class="s">&quot;winter.m&quot;</span><span class="p">;</span>

<span class="n">WinterRun</span><span class="o">:</span> <span class="kt">module</span> <span class="p">{</span>
    <span class="n">init</span><span class="o">:</span> <span class="kt">fn</span><span class="p">(</span><span class="n">ctx</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">,</span> <span class="n">args</span><span class="o">:</span> <span class="kt">list</span> <span class="kt">of</span> <span class="kt">string</span><span class="p">);</span>
<span class="p">};</span>

<span class="n">init</span><span class="p">(</span><span class="kc">nil</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">,</span> <span class="n">args</span><span class="o">:</span> <span class="kt">list</span> <span class="kt">of</span> <span class="kt">string</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">sys</span> <span class="o">:=</span> <span class="k">load</span> <span class="n">Sys</span> <span class="n">Sys</span><span class="o">-&gt;</span><span class="n">PATH</span><span class="p">;</span>
    <span class="n">winter</span> <span class="o">:=</span> <span class="k">load</span> <span class="n">Winter</span> <span class="n">Winter</span><span class="o">-&gt;</span><span class="n">PATH</span><span class="p">;</span>

    <span class="k">if</span> <span class="p">(</span><span class="k">len</span> <span class="n">args</span> <span class="o">&lt;</span> <span class="mi">2</span><span class="p">)</span> <span class="p">{</span>
        <span class="n">sys</span><span class="o">-&gt;</span><span class="n">fprint</span><span class="p">(</span><span class="n">sys</span><span class="o">-&gt;</span><span class="n">fildes</span><span class="p">(</span><span class="mi">2</span><span class="p">),</span> <span class="s">&quot;usage: winter &lt;file&gt; [args...]</span><span class="se">\n</span><span class="s">&quot;</span><span class="p">);</span>
        <span class="k">return</span><span class="p">;</span>
    <span class="p">}</span>

    <span class="n">wargs</span> <span class="o">:=</span> <span class="k">tl</span> <span class="n">args</span><span class="p">;</span>
    <span class="n">winter</span><span class="o">-&gt;</span><span class="n">run</span><span class="p">(</span><span class="k">hd</span> <span class="n">wargs</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>

<p>
Currently, only the path to the compiled script file is passed to the
interpreter.
</p>

<p>
An example script file looks like this:
</p>

<pre>
include "lib/term.sr"
include "lib/textio.sr"

def main()
    init_terminal()
    write_string("Hello world!\n")
end
</pre>

<p>
After compiling it to opcodes and writing them to a file, it can be run in
Inferno, like this:
</p>

<pre>
; winter /tmp/out.w
Hello world!
</pre>

<p>
Since the file descriptors were set up by the Limbo wrapper module, output can
be handled in the usual way:
</p>

<pre>
; winter /tmp/out.w | xd
0000000 48656c6c 6f20776f 726c6421 0a000000
000000d
</pre>

<p>
The next step is to implement support for system calls so that files and
directories can be interacted with.
</p>

<!-- /content -->
12345678901234567890123456789012345678901234567890123456789012345678901234567890
<!-- name -->David Boddie<!-- /name -->
<!-- date -->3 February 2024<!-- /date -->
