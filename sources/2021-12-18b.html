<!-- title -->Modules<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- content -->
<p>
I wanted to avoid writing a large single Limbo file containing all the code for
new types. That means putting code into a module and using it from the main
program. If you have already learned Python, using modules in Limbo is quite
confusing. I find that I need to think more in terms of C programming,
especially when writing my own modules.
</p>

<h2>Creating a new module</h2>

<p>
I created a new module for the <tt>Label</tt> type, calling the module
<tt>Panels</tt> and creating the <a href="code/2021-12-18-modules/panels.m">panels.m</a>
file to contain declarations for all the types and functions it exposes:
</p>

<div class="highlight"><pre><span></span><span class="n">Panels</span><span class="p">:</span> <span class="n">module</span>
<span class="p">{</span>
    <span class="n">PATH</span><span class="p">:</span> <span class="n">con</span> <span class="s">&quot;panels.dis&quot;</span><span class="p">;</span>

    <span class="n">default_font</span><span class="p">:</span> <span class="n">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Font</span><span class="p">;</span>
    <span class="nb">display</span><span class="p">:</span> <span class="n">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Display</span><span class="p">;</span>

    <span class="n">init</span><span class="p">:</span> <span class="n">fn</span><span class="p">(</span><span class="n">ctx</span><span class="p">:</span> <span class="n">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">);</span>

    <span class="n">Label</span><span class="p">:</span> <span class="n">adt</span>
    <span class="p">{</span>
        <span class="n">left</span><span class="p">,</span> <span class="n">centre</span><span class="p">,</span> <span class="n">right</span><span class="p">:</span> <span class="n">con</span> <span class="n">iota</span><span class="p">;</span>

        <span class="nb">text</span><span class="p">:</span> <span class="n">string</span><span class="p">;</span>
        <span class="n">font</span><span class="p">:</span> <span class="n">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Font</span><span class="p">;</span>
        <span class="n">alignment</span><span class="p">:</span> <span class="n">int</span><span class="p">;</span>

        <span class="n">draw</span><span class="p">:</span> <span class="n">fn</span><span class="p">(</span><span class="n">label</span><span class="p">:</span> <span class="n">self</span> <span class="n">ref</span> <span class="n">Label</span><span class="p">,</span> <span class="n">rect</span><span class="p">:</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Rect</span><span class="p">);</span>
    <span class="p">};</span>
<span class="p">};</span>
</pre></div>

<p>
The <tt>PATH</tt> definition is a bit unusual for a Limbo module in that it
references a file using a relative path&nbsp;&mdash;&nbsp;<tt>panels.dis</tt>
will be located in the same directory as the files that use it. Normally, the
location for a compiled module would be in the <tt>/dis</tt> directory and, in
this case, would be <tt>/dis/panels.dis</tt> or something similar.
</p>

<p>
The <tt>panels.m</tt> file doesn't include or load any other modules. Most
module interface files in the <tt>/module</tt> directory are like this. It is
up to a Limbo file that includes the module to ensure that other modules needed by
it are included before it is itself included. The declarations in the module
interface file are interpreted in the context of the program including them.
</p>

<p>
The code for the <tt>Panels</tt> module itself
(<a href="code/2021-12-18-modules/panels.b">panels.b</a>) includes the
<tt>panels.m</tt> file after other modules so that the types it refers to are
already defined.
</p>

<div class="highlight"><pre><span></span><span class="k">implement</span> <span class="n">Panels</span><span class="p">;</span>

<span class="k">include</span> <span class="s">&quot;sys.m&quot;</span><span class="p">;</span>
<span class="k">include</span> <span class="s">&quot;draw.m&quot;</span><span class="p">;</span>
<span class="n">Display</span><span class="p">,</span> <span class="n">Font</span><span class="p">,</span> <span class="n">Image</span><span class="p">,</span> <span class="n">Rect</span><span class="o">:</span> <span class="k">import</span> <span class="n">draw</span><span class="p">;</span>

<span class="k">include</span> <span class="s">&quot;panels.m&quot;</span><span class="p">;</span>

<span class="n">draw</span><span class="o">:</span> <span class="n">Draw</span><span class="p">;</span>
</pre></div>

<p>
The <tt>Label.draw</tt> method now appears in this module, and has been
extended to accept <tt>nil</tt> as a value for the font, which causes the
function to use the default font.
</p>

<div class="highlight"><pre><span class="n">Label</span><span class="p">.</span><span class="n">draw</span><span class="p">(</span><span class="n">label</span><span class="o">:</span> <span class="kt">self</span> <span class="kt">ref</span> <span class="n">Label</span><span class="p">,</span> <span class="n">rect</span><span class="o">:</span> <span class="n">Rect</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">x</span><span class="o">:</span> <span class="kt">int</span><span class="p">;</span>
    <span class="n">font</span> <span class="o">:=</span> <span class="n">label</span><span class="p">.</span><span class="n">font</span><span class="p">;</span>
    <span class="k">if</span> <span class="p">(</span><span class="n">font</span> <span class="o">==</span> <span class="kc">nil</span><span class="p">)</span>
        <span class="n">font</span> <span class="o">=</span> <span class="n">default_font</span><span class="p">;</span>

    <span class="k">case</span> <span class="n">label</span><span class="p">.</span><span class="n">alignment</span> <span class="p">{</span>
    <span class="n">Label</span><span class="p">.</span><span class="n">left</span> <span class="o">=&gt;</span>
        <span class="n">x</span> <span class="o">=</span> <span class="n">rect</span><span class="p">.</span><span class="n">min</span><span class="p">.</span><span class="n">x</span><span class="p">;</span>
    <span class="n">Label</span><span class="p">.</span><span class="n">centre</span> <span class="o">=&gt;</span>
        <span class="n">width</span> <span class="o">:=</span> <span class="n">label</span><span class="p">.</span><span class="n">font</span><span class="p">.</span><span class="n">width</span><span class="p">(</span><span class="n">label</span><span class="p">.</span><span class="n">text</span><span class="p">);</span>
        <span class="n">x</span> <span class="o">=</span> <span class="n">rect</span><span class="p">.</span><span class="n">min</span><span class="p">.</span><span class="n">x</span> <span class="o">+</span> <span class="p">(</span><span class="n">rect</span><span class="p">.</span><span class="n">dx</span><span class="p">()</span> <span class="o">-</span> <span class="n">width</span><span class="p">)</span> <span class="o">/</span> <span class="mi">2</span><span class="p">;</span>
    <span class="n">Label</span><span class="p">.</span><span class="n">right</span> <span class="o">=&gt;</span>
        <span class="n">width</span> <span class="o">:=</span> <span class="n">label</span><span class="p">.</span><span class="n">font</span><span class="p">.</span><span class="n">width</span><span class="p">(</span><span class="n">label</span><span class="p">.</span><span class="n">text</span><span class="p">);</span>
        <span class="n">x</span> <span class="o">=</span> <span class="n">rect</span><span class="p">.</span><span class="n">max</span><span class="p">.</span><span class="n">x</span> <span class="o">-</span> <span class="n">width</span><span class="p">;</span>
    <span class="o">*</span> <span class="o">=&gt;</span>
        <span class="n">x</span> <span class="o">=</span> <span class="n">rect</span><span class="p">.</span><span class="n">min</span><span class="p">.</span><span class="n">x</span><span class="p">;</span>
    <span class="p">}</span>

    <span class="n">display</span><span class="p">.</span><span class="n">image</span><span class="p">.</span><span class="n">text</span><span class="p">((</span><span class="n">x</span><span class="p">,</span> <span class="n">rect</span><span class="p">.</span><span class="n">min</span><span class="p">.</span><span class="n">y</span><span class="p">),</span> <span class="n">display</span><span class="p">.</span><span class="n">white</span><span class="p">,</span> <span class="p">(</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">),</span> <span class="n">label</span><span class="p">.</span><span class="n">font</span><span class="p">,</span>
                       <span class="n">label</span><span class="p">.</span><span class="n">text</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>

<p>
An <tt>init</tt> function is provided for the module as a whole. It creates a
display and sets up any common resources&nbsp;&mdash;&nbsp;currently just the
default font. It is up to the user of the module to call this function before
using any other features of the module.
</p>

<div class="highlight"><pre><span class="n">init</span><span class="p">(</span><span class="n">ctx</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">draw</span> <span class="o">=</span> <span class="k">load</span> <span class="n">Draw</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">PATH</span><span class="p">;</span>
    <span class="n">display</span> <span class="o">=</span> <span class="n">Display</span><span class="p">.</span><span class="n">allocate</span><span class="p">(</span><span class="kc">nil</span><span class="p">);</span>
    <span class="n">default_font</span> <span class="o">=</span> <span class="n">Font</span><span class="p">.</span><span class="n">open</span><span class="p">(</span><span class="n">display</span><span class="p">,</span> <span class="s">&quot;*default*&quot;</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>

<p>
With the <tt>panels.b</tt> and <tt>panels.m</tt> files in place, let's look at
a simple program that uses the module.
</p>

<h2>Using the module</h2>

<p>
The <a href="code/2021-12-18-modules/labels.b">labels.b</a> module recreates
the previous example, importing the <tt>panels.m</tt> file after other module
interface files to avoid compiler errors.
</p>

<div class="highlight"><pre><span></span><span class="k">implement</span> <span class="n">Test</span><span class="p">;</span>

<span class="k">include</span> <span class="s">&quot;sys.m&quot;</span><span class="p">;</span>
<span class="k">include</span> <span class="s">&quot;draw.m&quot;</span><span class="p">;</span>
<span class="n">draw</span><span class="o">:</span> <span class="n">Draw</span><span class="p">;</span>
<span class="c1"># These imports are needed to allow method calls on these types:</span>
<span class="n">Display</span><span class="p">,</span> <span class="n">Image</span><span class="p">,</span> <span class="n">Rect</span><span class="o">:</span> <span class="k">import</span> <span class="n">draw</span><span class="p">;</span>

<span class="k">include</span> <span class="s">&quot;panels.m&quot;</span><span class="p">;</span>
<span class="n">panels</span><span class="o">:</span> <span class="n">Panels</span><span class="p">;</span>
<span class="n">default_font</span><span class="p">,</span> <span class="n">display</span><span class="p">,</span> <span class="n">Label</span><span class="o">:</span> <span class="k">import</span> <span class="n">panels</span><span class="p">;</span>

<span class="n">Test</span><span class="o">:</span> <span class="kt">module</span>
<span class="p">{</span>
    <span class="n">init</span><span class="o">:</span> <span class="kt">fn</span><span class="p">(</span><span class="kc">nil</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">,</span> <span class="kc">nil</span><span class="o">:</span> <span class="kt">list</span> <span class="kt">of</span> <span class="kt">string</span><span class="p">);</span>
<span class="p">};</span>
</pre></div>

<p>
The <tt>init</tt> function is similar to before, except that the <tt>Panels</tt>
module needs to be loaded and initialised before use.
</p>

<div class="highlight"><pre><span class="n">init</span><span class="p">(</span><span class="n">context</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">,</span> <span class="kc">nil</span><span class="o">:</span> <span class="kt">list</span> <span class="kt">of</span> <span class="kt">string</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">panels</span> <span class="o">=</span> <span class="k">load</span> <span class="n">Panels</span> <span class="n">Panels</span><span class="o">-&gt;</span><span class="n">PATH</span><span class="p">;</span>
    <span class="n">draw</span> <span class="o">=</span> <span class="k">load</span> <span class="n">Draw</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">PATH</span><span class="p">;</span>
    
    <span class="c1"># Initialise the panels instance.</span>
    <span class="n">panels</span><span class="o">-&gt;</span><span class="n">init</span><span class="p">(</span><span class="n">context</span><span class="p">);</span>

    <span class="c1"># Draw the background, filling the entire screen.</span>
    <span class="n">display</span><span class="p">.</span><span class="n">image</span><span class="p">.</span><span class="n">draw</span><span class="p">(</span><span class="n">Rect</span><span class="p">((</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">),</span> <span class="p">(</span><span class="mi">320</span><span class="p">,</span><span class="mi">240</span><span class="p">)),</span> <span class="n">display</span><span class="p">.</span><span class="n">rgb</span><span class="p">(</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">,</span><span class="mi">64</span><span class="p">),</span>
                       <span class="kc">nil</span><span class="p">,</span> <span class="p">(</span><span class="mi">0</span><span class="p">,</span><span class="mi">0</span><span class="p">));</span>

    <span class="c1"># Create labels to show the text with different alignments.</span>
    <span class="n">left_label</span> <span class="o">:=</span> <span class="kt">ref</span> <span class="n">Label</span><span class="p">(</span><span class="s">&quot;Left&quot;</span><span class="p">,</span> <span class="n">default_font</span><span class="p">,</span> <span class="n">Label</span><span class="p">.</span><span class="n">left</span><span class="p">);</span>
    <span class="n">centre_label</span> <span class="o">:=</span> <span class="kt">ref</span> <span class="n">Label</span><span class="p">(</span><span class="s">&quot;Centre&quot;</span><span class="p">,</span> <span class="n">default_font</span><span class="p">,</span> <span class="n">Label</span><span class="p">.</span><span class="n">centre</span><span class="p">);</span>
    <span class="n">right_label</span> <span class="o">:=</span> <span class="kt">ref</span> <span class="n">Label</span><span class="p">(</span><span class="s">&quot;Right&quot;</span><span class="p">,</span> <span class="n">default_font</span><span class="p">,</span> <span class="n">Label</span><span class="p">.</span><span class="n">right</span><span class="p">);</span>

    <span class="c1"># Draw the labels.</span>
    <span class="n">left_label</span><span class="p">.</span><span class="n">draw</span><span class="p">(</span><span class="n">Rect</span><span class="p">((</span><span class="mi">2</span><span class="p">,</span><span class="mi">50</span><span class="p">),</span> <span class="p">(</span><span class="mi">318</span><span class="p">,</span><span class="mi">90</span><span class="p">)));</span>
    <span class="n">centre_label</span><span class="p">.</span><span class="n">draw</span><span class="p">(</span><span class="n">Rect</span><span class="p">((</span><span class="mi">2</span><span class="p">,</span><span class="mi">100</span><span class="p">),</span> <span class="p">(</span><span class="mi">318</span><span class="p">,</span><span class="mi">140</span><span class="p">)));</span>
    <span class="n">right_label</span><span class="p">.</span><span class="n">draw</span><span class="p">(</span><span class="n">Rect</span><span class="p">((</span><span class="mi">2</span><span class="p">,</span><span class="mi">150</span><span class="p">),</span> <span class="p">(</span><span class="mi">318</span><span class="p">,</span><span class="mi">190</span><span class="p">)));</span>
<span class="p">}</span>
</pre></div>

<p>
Having a module to hold user interface types should hopefully make it easier to
organise the code more effectively and write more concise programs.
</p>
<!-- /content -->

<!-- name -->David Boddie<!-- /name -->
<!-- date -->18 December 2021<!-- /date -->
