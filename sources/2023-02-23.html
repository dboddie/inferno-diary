<!-- title -->Blinking an LED (again)<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- content -->

<p>
Back in January last year I wrote a <a href="2022-01-30a.html">simple
blinking LED example</a> for an STM32 microcontroller using Inferno's C compiler.
Since then I've been working on getting Inferno itself to run on the
microcontroller. That was one of the reasons I was interesting in getting into
microcontrollers&nbsp;&mdash;&nbsp;the other being that the <a
href="https://www.sparkfun.com/micromod#processor_boards">MicroMod processor
boards</a> are so cute!
</p>

<h2>A long break</h2>

<p>
I tried building a kernel in February last year, starting by getting the clock
functions set up and enabling a UART to help with debugging. This relied
heavily on the bare metal work I'd put in beforehand. I put a lot of effort
into getting support for preemption into what I hoped was a basically working
state. Despite this, I kept on running into problems where the memory allocator
would fall over with what looked like corruption. Tweaking memory usage didn't
really help, and I put the whole effort on hold while I went back to
<a href="2022-08-09.html">bare metal coding</a>.
</p>

<p>
Later in 2022, I picked up the port again and tried to figure out if I could
make it work. Even though I'd had a sort-of-running system, it didn't encourage
me to play with it when it could crash any time I ran a program. After much
flailing around, I found that I hadn't done enough work on the preemption code
to preserve processor state, and fixing this made the system a lot more stable.
With something a bit more robust, it became possible to bring some device
drivers over from my bare metal code and write some Limbo programs.
</p>

<h2>Namespace and devices</h2>

<p>
When Inferno boots on the development board, the init process sets up a
namespace that includes a fairly minimal set of devices in the <tt>/dev</tt>
directory:
</p>

<pre>
Initial Dis: "/osinit.dis"
**
** Inferno
** Vita Nuova
**
stm32f405$ ls /dev | mc
ink            hostowner      memory         sysname
cons           keyboard       msec           time
consctl        klog           null           user
sysctl         kprint         random         jit
drivers        scancode       notquiterandom leds
</pre>

<p>
You can see the commands that can be used to reconstruct this namespace by
running the <tt>ns</tt> command:
</p>

<pre>
stm32f405$ ns
bind /dev /dev
bind -a '#c' /dev
bind -a '#L' /dev
bind /dev/ink /dev/ink
bind -a '#u' /dev/ink
bind -c '#e' /env
bind '#p' /prog
bind '#d' /fd
cd /
</pre>

<p>
The <tt>/dev/leds</tt> file can be read from and written to. When you read from
it, you get the current state of the status LED on the development board.
</p>

<pre>
stm32f405$ cat /dev/leds
0
</pre>

<p>
You can also write a number to it to change its state: 0 for off, 1 for on.
Like this:
</p>

<pre>
stm32f405$ echo 1 > /dev/leds
stm32f405$ echo 0 > /dev/leds
</pre>

<p>
With this kind of interface to the LED, we can write a Limbo program to make it
blink on and off.
</p>

<h2>Blink</h2>

<p>
This is what the blink program looks like:
</p>

<div class="highlight"><pre><span></span><span class="k">implement</span> <span class="n">Blink</span><span class="p">;</span>

<span class="k">include</span> <span class="s">&quot;draw.m&quot;</span><span class="p">;</span>
<span class="k">include</span> <span class="s">&quot;sys.m&quot;</span><span class="p">;</span>
<span class="n">sys</span><span class="o">:</span> <span class="n">Sys</span><span class="p">;</span>

<span class="n">Blink</span><span class="o">:</span> <span class="kt">module</span>
<span class="p">{</span>
    <span class="n">init</span><span class="o">:</span>   <span class="kt">fn</span><span class="p">(</span><span class="kc">nil</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">,</span> <span class="n">args</span><span class="o">:</span> <span class="kt">list</span> <span class="kt">of</span> <span class="kt">string</span><span class="p">);</span>
<span class="p">};</span>

<span class="n">init</span><span class="p">(</span><span class="kc">nil</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">,</span> <span class="n">args</span><span class="o">:</span> <span class="kt">list</span> <span class="kt">of</span> <span class="kt">string</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">sys</span> <span class="o">=</span> <span class="k">load</span> <span class="n">Sys</span> <span class="n">Sys</span><span class="o">-&gt;</span><span class="n">PATH</span><span class="p">;</span>

    <span class="n">off</span> <span class="o">:=</span> <span class="kt">array</span> <span class="kt">of</span> <span class="kt">byte</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">sprint</span><span class="p">(</span><span class="s">&quot;%d\n</span><span class="s">&quot;</span><span class="p">,</span> <span class="mi">0</span><span class="p">);</span>
    <span class="n">on</span> <span class="o">:=</span> <span class="kt">array</span> <span class="kt">of</span> <span class="kt">byte</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">sprint</span><span class="p">(</span><span class="s">&quot;%d\n</span><span class="s">&quot;</span><span class="p">,</span> <span class="mi">1</span><span class="p">);</span>
    <span class="n">states</span> <span class="o">:=</span> <span class="kt">array</span><span class="p">[</span><span class="mi">2</span><span class="p">]</span> <span class="kt">of</span> <span class="p">{</span> <span class="n">off</span><span class="p">,</span> <span class="n">on</span> <span class="p">};</span>
    <span class="n">i</span> <span class="o">:=</span> <span class="mi">0</span><span class="p">;</span>

    <span class="n">for</span> <span class="p">(;;)</span> <span class="p">{</span>
        <span class="n">set_state</span><span class="p">(</span><span class="n">states</span><span class="p">[</span><span class="n">i</span><span class="p">]);</span>
        <span class="n">sys</span><span class="o">-&gt;</span><span class="n">sleep</span><span class="p">(</span><span class="mi">1000</span><span class="p">);</span>
        <span class="n">i</span> <span class="o">=</span> <span class="mi">1</span> <span class="o">-</span> <span class="n">i</span><span class="p">;</span>
    <span class="p">}</span>
<span class="p">}</span>

<span class="n">set_state</span><span class="p">(</span><span class="n">s</span><span class="o">:</span> <span class="kt">array</span> <span class="kt">of</span> <span class="kt">byte</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">fd</span> <span class="o">:=</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">open</span><span class="p">(</span><span class="s">&quot;/dev/leds&quot;</span><span class="p">,</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">OWRITE</span><span class="p">);</span>
    <span class="k">if</span> <span class="p">(</span><span class="n">fd</span> <span class="o">!=</span> <span class="kc">nil</span><span class="p">)</span>
        <span class="n">sys</span><span class="o">-&gt;</span><span class="n">write</span><span class="p">(</span><span class="n">fd</span><span class="p">,</span> <span class="n">s</span><span class="p">,</span> <span class="k">len</span> <span class="n">s</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>

<p>
In the first version of the program I changed the state of the LED in the loop
in the <tt>main</tt> function. This had the unexpected behaviour of not
blinking the LED at all. I soon realised that the device would only receive the
data after the file was closed, and there's no explicit way to do that in Limbo.
The result is the use of the <tt>set_state</tt> function to ensure that the
file descriptor is garbage-collected.
</p>

<p>
The result of this is that the status LED should change state every second or so.
</p>
<!-- /content -->

<!-- name -->David Boddie<!-- /name -->
<!-- date -->23 February 2023<!-- /date -->
