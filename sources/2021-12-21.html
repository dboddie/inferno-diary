<!-- title -->Abstractions, Keyboard Handling and Drawing<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- content -->
<p>
When doing things with user interface widgets I find that I tend to over-think
things, making them more complicated than they need to be. It's too easy to
take a step back and try to create a general framework when the problem at hand
is fairly simple. I suppose that I want to avoid making decisions that will
limit the usefulness of what I write, resulting in code needing to be replaced
later.
</p>

<h2>Abstractions</h2>

<p>
Limbo provides abstract data types (ADTs) which are a bit like structs in C but
with member functions. For someone who has used object-oriented frameworks in
C++ and Python, it's challenging to adapt to a different way of describing and
expressing user interface elements. I've tried to handle this before in
different ways, usually falling back on the <em>pick adt</em> feature in Limbo
that lets you define what are effectively subtypes of a data type and select
the relevant one at run-time.
</p>

<p>
In the following code, assigning <tt>widget</tt> (with type <tt>Widget</tt>) to
<tt>inst</tt> in a <tt>pick</tt> statement lets the program handle the
actual type of <tt>widget</tt> via <tt>inst</tt>. 
</p>

<div class="highlight"><pre><span></span><span class="n">pick</span> <span class="n">inst</span> <span class="o">:=</span> <span class="n">widget</span>
<span class="p">{</span>
    <span class="n">Label</span> <span class="o">=&gt;</span>
        <span class="n">widget</span><span class="p">.</span><span class="n">drawtext</span><span class="p">(</span><span class="n">inst</span><span class="p">.</span><span class="n">text</span><span class="p">,</span> <span class="n">inst</span><span class="p">.</span><span class="n">alignment</span><span class="p">,</span> <span class="n">inst</span><span class="p">.</span><span class="n">font</span><span class="p">,</span> <span class="n">im</span><span class="p">);</span>

    <span class="n">LineEdit</span> <span class="o">=&gt;</span>
        <span class="n">widget</span><span class="p">.</span><span class="n">drawtext</span><span class="p">(</span><span class="n">inst</span><span class="p">.</span><span class="n">text</span><span class="p">,</span> <span class="n">inst</span><span class="p">.</span><span class="n">alignment</span><span class="p">,</span> <span class="n">inst</span><span class="p">.</span><span class="n">font</span><span class="p">,</span> <span class="n">im</span><span class="p">);</span>
        <span class="n">widget</span><span class="p">.</span><span class="n">drawcursor</span><span class="p">(</span><span class="n">inst</span><span class="p">.</span><span class="n">text</span><span class="p">,</span> <span class="n">inst</span><span class="p">.</span><span class="n">alignment</span><span class="p">,</span> <span class="n">inst</span><span class="p">.</span><span class="n">font</span><span class="p">,</span>
                          <span class="n">inst</span><span class="p">.</span><span class="n">cursorpos</span><span class="p">,</span> <span class="n">im</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>

<p>
Each of the possibilities is handled in a similar way to a <tt>case</tt>
statement, with the type of <tt>inst</tt> selecting which group of statements
to execute. So, if the object actually has the type <tt>Widget.Label</tt> then
<tt>inst</tt> is given that type and the first group of statements are executed.
</p>

<p>
Accessing the subtype-specific features of <tt>widget</tt> like this is a bit
like casting to more specific types in C++ but in a way that covers multiple
possibilities in a statement, and which seems less error-prone.
</p>

<h2>Keyboard handling</h2>

<p>
The application I want to write will need to access the <tt>/dev/keyboard</tt>
device to accept keyboard input. The code to do this is similar to some I found
in Inferno's <tt>/appl/lib/wmlib.b</tt> file. My version of the code sends key
codes to a widget's <tt>key</tt> method, but the basic file reading and
conversion is unchanged.
</p>

<div class="highlight"><pre><span></span><span class="n">f</span> <span class="o">:=</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">open</span><span class="p">(</span><span class="s">&quot;/dev/keyboard&quot;</span><span class="p">,</span> <span class="n">Sys</span><span class="o">-&gt;</span><span class="n">OREAD</span><span class="p">);</span>
<span class="n">buf</span> <span class="o">:=</span> <span class="kt">array</span><span class="p">[</span><span class="mi">12</span><span class="p">]</span> <span class="kt">of</span> <span class="kt">byte</span><span class="p">;</span>

<span class="n">for</span> <span class="p">(;;)</span> <span class="p">{</span>
    <span class="k">while</span> <span class="p">((</span><span class="n">n</span> <span class="o">:=</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">read</span><span class="p">(</span><span class="n">f</span><span class="p">,</span> <span class="n">buf</span><span class="p">,</span> <span class="k">len</span> <span class="n">buf</span><span class="p">))</span> <span class="o">&gt;</span> <span class="mi">0</span><span class="p">)</span> <span class="p">{</span>
        <span class="n">s</span> <span class="o">:=</span> <span class="kt">string</span> <span class="n">buf</span><span class="p">[</span><span class="o">:</span><span class="n">n</span><span class="p">];</span>
        <span class="n">for</span> <span class="p">(</span><span class="n">i</span> <span class="o">:=</span> <span class="mi">0</span><span class="p">;</span> <span class="n">i</span> <span class="o">&lt;</span> <span class="k">len</span> <span class="n">s</span><span class="p">;</span> <span class="n">i</span><span class="o">++</span><span class="p">)</span>
            <span class="n">edit</span><span class="p">.</span><span class="n">key</span><span class="p">(</span><span class="n">s</span><span class="p">[</span><span class="n">i</span><span class="p">]);</span>

        <span class="n">edit</span><span class="p">.</span><span class="n">draw</span><span class="p">();</span>
    <span class="p">}</span>
<span class="p">}</span>
</pre></div>

<p>
The critical part is performing the conversion of input bytes into a string
and extracting the characters individually as integers. If it works for
<tt>wm</tt> then it should be good enough for my application!
</p>

<h2>Drawing</h2>

<p>
I often spend a lot of time trying to figure out how to use the <tt>Draw</tt>
API, relying on some memory of how it is supposed to be used. After some
experimentation, I managed to get some basic widgets up and running.
</p>

<div class="centre"><img class="dark" src="images/2021-12-21-line-edit.png" /><br/>
<span class="caption">A simple line edit widget for entering text</span></div>

<p>
The keyboard handling code is currently in the main program, and the widgets
aren't really grouped or organised in any meaningful way. That will be the next
thing to do.
</p>

<!-- /content -->

<!-- name -->David Boddie<!-- /name -->
<!-- date -->21 December 2021<!-- /date -->
