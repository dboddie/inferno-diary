<!-- title -->I2C<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- summary -->
Implementing I2C communication in Inferno running on the SparkFun Artemis
module, then driving a couple of devices using the the I2C bus.
<!-- /summary -->

<!-- content -->

<p>
Previously, I looked into <a href="2023-08-22.html">porting Inferno to the SparkFun
Artemis module</a>, which uses the Apollo3 microcontroller. It's a bit more
convenient to use than some of the other MCUs in the MicroMod series of processor
boards because it exposes one of its two UARTs via the standard USB connection that
the MicroMod carrier boards have, making it fairly straightforward to turn it into
a plug-and-play development board.</p>

<h2>Peripherals</h2>

<p>
With the status LED and UARTs functioning, there are plenty of other tasks that
could be started right now. The port doesn't support floating point operations
fully, there's no functioning just-in-time (JIT) compiler, and the Thumb-2
compiler that is used to build it could use some tidying up. But it would be
nice to explore a bit more before tackling those issues.
</p>

<p>
Since I had transferred the processor board to a
<a href="https://www.sparkfun.com/products/16829">Data Logging Carrier Board</a>,
there are two input/output peripherals that are just waiting to be supported:
I<sup>2</sup>C and SPI. The Apollo3 MCU handles both of these together, and it
looked like I<sup>2</sup>C might be easier to start with, so that's where things
went next.
</p>

<h2>Back to bare metal</h2>

<p>
Once you get past a certain point with porting Inferno, it becomes fairly
comfortable to write drivers to support peripherals. Still, I like to return
to my bare metal development environment because it's quicker to create
and flash small programs onto a development board, and also because I might
want to write small standalone programs that run directly on the hardware.
</p>

<p>
Like the UARTs, setting up I2C involved a certain amount of trial and error,
decoding what the datasheet was trying to tell me about FIFOs, commands,
clocks, DMA, with the steps needed scattered around in the chapter about the
IOM (I2C/SPI Master Module). There is also a certain amount of basic information
mixed in about I<sup>2</sup>C which is potentially useful for anyone unfamiliar
with it, but distracting otherwise.
</p>

<p>
Ultimately, it all boils down to the usual dance of enabling power to the
peripheral, configuring pads and pins, then configuring the clock used for the
peripheral. Additionally, the appropriate submodule within the IOM peripheral
needs to be enabled. After that, it was a case of writing tests and example code
to verify that things worked at a basic level, using a couple of I<sup>2</sup>C
devices.
</p>

<h2>The Inferno i2c device</h2>

<p>
Inferno already has a
<a href="https://www.vitanuova.com/inferno/man/3/i2c.html">file server for
accessing I<sup>2</sup>C devices</a>, providing an interface that programs can use
to interact with them. The file server presents a standard way to access devices
and performs a lot of the housekeeping needed to manage file operations,
particularly those that configure how the interface works.
</p>

<p>
Underneath the file server, drivers for the specific hardware peripherals in use
need to be written to interact with devices, and this means that a common API
needs to be implemented.
</p>

<p><tt>void i2csetup(int polling)</tt> performs setup of the hardware peripheral
used to access I<sup>2</sup>C devices. If <tt>polling</tt> is 1, the setup will
involve configuring interrupts.
In my implementation, I just ignored this. The <tt>i2c</tt> device appears to
send 0 to this function, in any case.</p>

<p><tt>long i2crecv(I2Cdev *d, void *buf, long len, ulong offset)</tt>
handles reads from the data file exposed for a device.</p>

<p><tt>long i2csend(I2Cdev *d, void *buf, long len, ulong offset)</tt>
handles writes to the data file exposed for a device.</p>

<p>
Once these are implemented, it becomes possible to use the <tt>i2c</tt> device
to bind an I<sup>2</sup>C device at a particular address to a target directory:
</p>

<pre>
apollo3$ bind -a '#J38' /dev
apollo3$ ls /dev | grep i2c
i2c.38.data
i2c.38.ctl
</pre>

<p>
There is some subtlety involved when using the files the <tt>i2c</tt> device
exposes to communicate with the I<sup>2</sup>C device itself. When you need to
send data to a particular device register, you first need to enable subaddressing
by sending a <tt>subaddress</tt> command to the control file, specifying the
size of the subaddress address space:
</p>

<pre>
apollo3$ echo 'subaddress 1' > /dev/i2c.38.ctl
</pre>

<p>
Now, it should be possible to seek to a particular offset in the data file and
perform a read or write operation. The offset will be translated to a register
address, or device subaddress, depending on your terminology.
</p>

<h2>I2C from Limbo</h2>

<p>
Although you can get quite far using the device files from the shell, it's easier
to use Limbo to interact with the data device file. The following two functions
show how file offsets are used to specify the register/subaddress to access.
</p>

<div class="highlight"><pre><span></span><span class="n">read_data</span><span class="p">(</span><span class="n">dfd</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">FD</span><span class="p">,</span> <span class="n">offset</span><span class="p">,</span> <span class="n">length</span><span class="o">:</span> <span class="kt">int</span><span class="p">)</span><span class="o">:</span> <span class="kt">array</span> <span class="kt">of</span> <span class="kt">byte</span>
<span class="p">{</span>
    <span class="n">b</span> <span class="o">:=</span> <span class="kt">array</span><span class="p">[</span><span class="n">length</span><span class="p">]</span> <span class="kt">of</span> <span class="kt">byte</span><span class="p">;</span>
    <span class="n">sys</span><span class="o">-&gt;</span><span class="n">seek</span><span class="p">(</span><span class="n">dfd</span><span class="p">,</span> <span class="kt">big</span> <span class="n">offset</span><span class="p">,</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">SEEKSTART</span><span class="p">);</span>
    <span class="n">sys</span><span class="o">-&gt;</span><span class="n">readn</span><span class="p">(</span><span class="n">dfd</span><span class="p">,</span> <span class="n">b</span><span class="p">,</span> <span class="n">length</span><span class="p">);</span>
    <span class="k">return</span> <span class="n">b</span><span class="p">;</span>
<span class="p">}</span>

<span class="n">write_data</span><span class="p">(</span><span class="n">dfd</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">FD</span><span class="p">,</span> <span class="n">offset</span><span class="o">:</span> <span class="kt">int</span><span class="p">,</span> <span class="n">b</span><span class="o">:</span> <span class="kt">array</span> <span class="kt">of</span> <span class="kt">byte</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">sys</span><span class="o">-&gt;</span><span class="n">seek</span><span class="p">(</span><span class="n">dfd</span><span class="p">,</span> <span class="kt">big</span> <span class="n">offset</span><span class="p">,</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">SEEKSTART</span><span class="p">);</span>
    <span class="n">sys</span><span class="o">-&gt;</span><span class="n">write</span><span class="p">(</span><span class="n">dfd</span><span class="p">,</span> <span class="n">b</span><span class="p">,</span> <span class="k">len</span> <span class="n">b</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>

<p>
It's possible to write simple tools to read from devices and write to others.
The first example I have to hand involves a Limbo program called <tt>aht</tt>
which reads from an <a href="https://learn.adafruit.com/adafruit-aht20">AHT20</a>
temperature and humidity sensor, writing the temperature to standard output.
The second program, <tt>ledsegment</tt>, reads a number from standard input
and converts it to data that it sends to a
<a href="https://shop.elecfreaks.com/products/elecfreaks-octopus-alphanumeric-led-brick">LED
brick</a>. The use of standard input and output means that we can connect these
programs with pipes to display the current temperature on the LED display:
</p>

<pre>
apollo3$ aht | ledsegment
</pre>

<p>
This might not all be as robust as it could be. Error handling and
resource management will no doubt need to be improved, but it's a proof
of concept, and another step toward more ambitious goals.
</p>

<p>
Code for the port can be found in the <tt>apollo3</tt> branch of
<a href="https://github.com/dboddie/inferno-os/tree/apollo3">this
repository</a>. Look in the <tt>os/apollo3/appl</tt> directory for
the programs mentioned above.
</p>

<!-- /content -->

<!-- name -->David Boddie<!-- /name -->
<!-- date -->29 August 2023<!-- /date -->
