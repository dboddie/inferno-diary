<!-- title -->Compiler hacks<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- summary -->
Hacking the Thumb compiler to save memory.
<!-- /summary -->

<!-- content -->
<p>
In the questions and discussion after my talk at <a href="https://iwp9.org">IWP9</a>,
there was a suggestion about modifying the toolchain to automatically mark
immutable/unmodified data so that the linker/loader could arrange for it to be
put into flash memory instead of RAM. This is definitely the best solution for
the use case that I'm interested in, and a step beyond what I was considering.
</p>

<h2>First step</h2>

<p>
My initial idea as a solution was to try and get the compiler to communicate
information about <tt>const</tt> data to the loader, <tt>tl</tt>, then annotate
structures and character arrays. Although the proposed solution is what I
really want, I decided to implement this approach first to learn more about how
the toolchain works.
</p>

<p>
The C compiler for Thumb code is <tt>tc</tt> which uses <tt>libcc</tt> for
common compiler features. Code for <tt>libcc</tt> resides in the <tt>utils/cc</tt>
directory. Since we are looking for machine-independent code that handles the
<tt>const</tt> keyword, we start by looking in <tt>cc</tt>.
</p>

<h2>Finding const in cc</h2>

<p>
The <tt>cc.y</tt> file contains the grammar for the compiler, to be fed to
<tt>yacc</tt> when the compiler is built in order to create <tt>y.tab.c</tt>.
The <tt>gname</tt> action indicates that <tt>const</tt>, <tt>volatile</tt> and
<tt>restrict</tt> are &ldquo;garbage words&rdquo;:
</p>

<pre>
gname:	/* garbage words */
	LCONSTNT { $$ = BCONSTNT; }
|	LVOLATILE { $$ = BVOLATILE; }
|	LRESTRICT { $$ = 0; }
</pre>

<p>
This reflects the comment in section 3.1 of the
<a href="https://www.vitanuova.com/inferno/papers/compiler.html">Plan 9 C
Compilers</a> document. However, looking in the <tt>sub.c</tt> file, we can see
that the <tt>constas</tt> function actually checks for assignments to
<tt>const</tt> symbols and warns if warnings are enabled.
</p>

<p>
We can trace the handling of the <tt>BCONSTNT</tt> value in <tt>sub.c</tt> to
the <tt>simpleg</tt> function where it is converted to <tt>GCONSTNT</tt> for
assignment to the <tt>garb</tt> member of a <tt>Type</tt> object. Other than
the check in the <tt>constas</tt> function, the information seems to be
discarded unless the <tt>garbt</tt> function is used to copy types, and this
doesn't happen outside the generated <tt>y.tab.c</tt> file. Instead, the
<tt>copytyp</tt> or <tt>typ</tt> functions are used, and any copied
<tt>const</tt> type is no longer <tt>const</tt>.
</p>

<p>
As an experiment, the <tt>typ</tt> function was modified to preserve the
&ldquo;garbage&rdquo; flags:
</p>

<pre>
Type*
typ(int et, Type *d)
{
        Type *t;

        t = alloc(sizeof(*t));
        t->etype = et;
        t->link = d;
        t->down = T;
        t->sym = S;
        t->width = ewidth[et];
        t->offset = 0;
        t->shift = 0;
        t->nbits = 0;
        t->garb = d ? d->garb : 0;
        return t;
}
</pre>

<p>
This information is now available to use later in the compilation process, but
we need it at the linking stage. This requires us to look at <tt>tc</tt> and
how object information is serialised.
</p>

<h2>Propagating const in tc</h2>

<p>
Taking a look at <tt>tc/swt.c</tt>, we find the <tt>outcode</tt> function which
writes symbol and program data to an object file. Since this doesn't output
any information about the <tt>const</tt>ness of the types associated with
symbols, we need to find a way to squeeze that information in somewhere.
The <tt>zname</tt> function writes symbol information, either in the form of
a sequence beginning with <tt>ANAME</tt> or <tt>ASIGNAME</tt>, so it looks like
a reasonable place to add a quick hack:
</p>

<pre>
        ...
        else if(s != nil && s->type != nil){
                bf[0] = ACONSTNAME;
                bf[1] = s->type->garb & GCONSTNT;
                bf[2] = t;        /* type */
                bf[3] = s->sym;   /* sym */
                Bwrite(b, bf, 4);
        }
        else{
                bf[0] = ANAME;
                bf[1] = t;        /* type */
                bf[2] = s->sym;   /* sym */
                Bwrite(b, bf, 3);
        }
        Bwrite(b, n, strlen(n)+1);
        ...
</pre>

<p>
We are only interested in propagating information about <tt>const</tt>,
introducing <tt>ACONSTNAME</tt> in <tt>tc/5.out.h</tt> to identify it in the
object file.
</p>

<p>
It would seem that the next step is to change <tt>tl</tt> to read in this
new information and process it.
</p>

<h2>Using const in tl</h2>

<p>
Object files are loaded by the <tt>ldobj</tt> function in the <tt>tl/obj.c</tt>
file. Normally, only the <tt>ANAME</tt> and <tt>ASIGNAME</tt> identifiers are
handled, but we add support for <tt>ACONSTNAME</tt> and record whether the
associated type is constant, storing the flag in an additional member of the
<tt>Sym</tt> struct (defined in <tt>tl/l.h</tt>).
</p>

<pre>
        ...
        if(o == ANAME || o == ASIGNAME || o == ACONSTNAME) {
                sig = 0;
                constant = 0;
                if(o == ASIGNAME){
                        sig = bloc[1] | (bloc[2]<<8) | (bloc[3]<<16) | (bloc[4]<<24);
                        bloc += 4;
                        c -= 4;
                } else if(o == ACONSTNAME) {
                        constant = bloc[1];
                        bloc += 1;
                        c -= 1;
                }
        ...
</pre>

<p>
Having collected the information we need, we can act on it in the <tt>dodata</tt>
function in <tt>tl/span.c</tt>, where existing code places string
constants into the text section of the resulting binary file:
</p>

<pre>
        ...
        if(debug['t']) {
                /*
                 * pull out string constants
                 */
                for(p = datap; p != P; p = p->link) {
                        s = p->from.sym;
                        if(p->to.type == D_SCONST)
                                s->type = SSTRING;
                        else if(s->constant != 0)
                                s->type = SSTRING;
                }
        }
        ...
</pre>

<p>
Adding the constant check to the symbol is enough to move data into the text
section. However, other tools also need to know about <tt>ACONSTNAME</tt>
because it isn't only <tt>tc</tt> and <tt>tl</tt> that will be handling
object files.
</p>

<h2>5c and libmach</h2>

<p>
General object file processing is performed by <tt>libmach</tt>, implemented in
<tt>utils/libmach</tt>. This needs to know about <tt>ACONSTNAME</tt>. What is
less obvious is that the <tt>libmach/5obj.c</tt> file includes <tt>5c/5.out.h</tt>
file, not the <tt>tc/5.out.h</tt> file we changed earlier, so these files need
to be synchronised.
</p>

<p>
The <tt>_read5</tt> function in <tt>libmach/5obj.c</tt> is updated with
similar code to that in <tt>tl/obj.c</tt>, except that this just skips past the
new information:
</p>

<pre>
        ...
        if(as == ANAME || as == ASIGNAME || as == ACONSTNAME){
                if(as == ASIGNAME){
                        Bread(bp, &p->sig, 4);
                        p->sig = leswal(p->sig);
                }else if(as == ACONSTNAME)
                        Bseek(bp, 1, 1);
        ...
</pre>

<p>
Perhaps it would be better if it handled the information somehow.
However, it doesn't appear to make much difference to the results of our
tests, and tools like <tt>iar</tt> can now handle the new data without crashing.
</p>

<h2>Finding structures</h2>

<p>
Referring back to the ideal plan of finding immutable structures, <tt>tl</tt>
has some idea about structures that can be considered to be constant.
As well as handling string constants and <tt>const</tt> data, we can add a
check for other potential constant data, printing symbol names and sizes:
</p>

<pre>
        ...
	if(debug['t']) {
		/*
		 * pull out string constants
		 */
                for(p = datap; p != P; p = p->link) {
                        s = p->from.sym;
                        if(p->to.type == D_SCONST)
                                s->type = SSTRING;
                        else if(s->constant != 0)
                                s->type = SSTRING;
                        else if(p->to.type == D_CONST)
                                print("XXX %s %d\n", s->name, s->value);
                }
        }
        ...
</pre>

<p>
This allows us to search and sort candidates, so that we can look for them and
annotate them with <tt>const</tt> as appropriate, running a sequence of
commands like this:
</p>

<pre>
$ mk nuke && mk | grep XXX | uniq | sort -k 3 -n | tail -n 10
</pre>

<p>
The result is a list of the ten largest candidates:
</p>

<pre>
XXX keysh 64
XXX roff 64
XXX sysctlcmd 72
XXX devtab 84
XXX progdbgcmd 96
XXX parity 128
XXX table 676
XXX rootdata 992
XXX optab 1024
XXX roottab 3472
</pre>

<p>
Many of these cannot simply be annotated with <tt>const</tt> because they need
to be modifiable at run-time. However, the symbol <tt>roff</tt> can be found in
<tt>os/cortexm/fpithumb.c</tt> is actually just a look-up table defined at
compile-time, so it can be made <tt>const</tt>, too.
</p>

<h2>Results</h2>

<p>
Using the current default configuration for the Apollo3 port, with a single
<tt>main</tt> memory pool for simplicity, the allocation summary (obtained with
the Ctrl-T, Ctrl-T, M debug key shortcut on the console) looks like this:
</p>

<pre>
    cursize     maxsize       hw         nalloc       nfree      nbrk       max      name
      57408      339968       57856         800         190          48      282548 main
</pre>

<p>
This shows about 332 KB of RAM available, of which 56 KB has already been used
at start up. I implemented another debug key shortcut (Ctrl-T, Ctrl-T, S) to
provide more information about memory usage:
</p>

<pre>
etext=9e320 bdata=10000000 edata=10007cc0 end=1000be60 mach=1005ef80
Total memory available: 332K
340096 bytes from 1000bf00 to 1005ef80
</pre>

<p>
Beyond the convenient summary of the total memory available, you should be able
to see that the data starts at 0x10000000 and ends at 0x10007cc0. This is a
region of RAM populated by data stored after the text (code) in the
kernel/system binary. The size of it, 0x7cc0, is about 31 KB. Following this is
uninitialised data, which ends at 0x1000be60, making the total RAM used around
47 KB. Since the system only has 384 KB of RAM, and the <tt>Mach</tt> data
structure is at the top of memory, this leaves around 332 KB free for the
system to use.
</p>

<p>
After annotating as many declarations as possible with <tt>const</tt>, either
directly or by modifying files like <tt>os/port/mkdevc</tt> and
<tt>os/port/portmkfile</tt>, we can rebuild Inferno and test the memory usage
again:
</p>

<pre>
    cursize     maxsize       hw         nalloc       nfree      nbrk       max      name
      57408      360704       57856         800         190          48      303284 main
</pre>

<p>
Now there is about 352 KB of RAM available: 20 KB more than before. Looking at
the other diagnostic, we can see that the end of initialised data is now only
0x2bd0 (11216) bytes above the start of RAM:
</p>

<pre>
etext=a3410 bdata=10000000 edata=10002bd0 end=10006d70 mach=1005ef80
Total memory available: 352K
360832 bytes from 10006e00 to 1005ef80
</pre>

<p>
Since the size of the uninitialised data is unchanged (about 16 KB) the saving
is purely from moving initialised data into flash memory. However, this brings
the total, initial RAM usage down to about 27 KB from 47 KB.
</p>

<h2>Thoughts</h2>

<p>
This fairly quick set of hacks to the toolchain saves a modest amount of RAM.
Although 20 KB doesn't seem like much of a gain, pushing the end of initialised
data below 32 KB feels like a milestone.
</p>

<p>
It would be good to get a better idea of what is left in the initialised data,
as well as finding out what contributes to the uninitialised data. It would
also be good to take another step with the toolchain to let the compiler do the
work of finding all the constant data.
</p>

<!-- /content -->
12345678901234567890123456789012345678901234567890123456789012345678901234567890
<!-- name -->David Boddie<!-- /name -->
<!-- date -->27 April 2024<!-- /date -->
