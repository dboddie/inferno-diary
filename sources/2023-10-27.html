<!-- title -->Porting Floating Point Support<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- summary -->
Enabling floating point support on the STM32F405.
<!-- /summary -->

<!-- content -->
<p>
Leaving the JIT work to one side for now, I wanted to see if I could bring the
floating point support code in the Apollo3 port to the STM32F405 port. One way
to do this would have been to mess around with git repositories, fetching and
pulling this way and that, then cleaning up the inevitable mess. Instead, I
brought across the changed files from the Apollo3 port and manually merged the
parts that needed merging.
</p>

<p>
Having to do all that work to introduce the changes to a different, though
related, port of Inferno has given me an opportunity to review what was done
over the last few weeks.
</p>

<h2>An overview</h2>

<p>
Changes were needed in various components to enable floating point operations:
</p>

<ul>
<li>The <tt>tc</tt>, <tt>5c</tt> compilers and <tt>tl</tt> linker needed
to be updated to support undefined instructions.</li>
<li>The endianness of stored floating point numbers needed to be updated.</li>
<li>Each port's configuration file needed to enable the <tt>math</tt> library
and <tt>math</tt> module.</li>
<li>The <tt>math</tt> library needed a workaround for a compiler issue.</li>
<li>A floating point emulator needed to be imported and modified.</li>
<li>Interrupt handling needed to be changed, with interrupt priorities used
instead of globally enabling and disabling interrupts.</li>
</ul>

<h2>General changes</h2>

<p>
Some changes were made to components outside the <tt>os</tt> directory which
are common to different ports and architectures.
</p>

<h3>Compilers</h3>

<p>
The <tt>tc</tt> compiler and <tt>tl</tt> linker were updated to generate
ARMv7-M floating point instructions and reflect the number of available
floating point registers. The temporary floating point register was updated in
both <tt>5c</tt> and <tt>tc</tt>.
</p>

<h3>Endianness of stored doubles</h3>

<p>
The <tt>Inferno/thumb/include/lib9.h</tt> file defines the endianness of stored
double floating point numbers. I changed this to specify little-endian doubles.
Elsewhere, the <tt>libkern</tt> library also encodes the endianness of doubles
for the <tt>_d2v</tt> function, and this needed updating.
</p>

<h3>Workaround for the math library</h3>

<p>
The <tt>libmath</tt> library includes a copy of <a href="https://netlib.org/fdlibm/">FDLIBM</a>
that doesn't play well with the compilers. There are notes in the <tt>readme</tt>
file for the library that some compilers had issues that required workarounds.
Inferno's Thumb C compiler has some issues
<a href="https://marc.info/?l=9fans&m=169558711314373&w=2">managing floating
point registers</a> when doubles are manipulated at the bit level. A quick
workaround is to disable &ldquo;registerization&rdquo; for the library in the
<tt>mkfile</tt>.
</p>

<h2>Port-specific changes</h2>

<p>
Some changes are more closely tied to the Thumb-2 ports and specific MCUs than
they are to general components, and these occur in the relevant <tt>os/*</tt>
directories. There is some overlap in how exceptions are handled and how
floating point instructions are decoded which makes it easier to implement
things on a per-port basis rather than trying to implement general solutions.
</p>

<h3>Unsupported instructions</h3>

<p>
Processors that don't support double precision floating point instructions need
an instruction emulator to perform those operations. The implementation in the
Raspberry Pi port of Inferno is in the <tt>fpiarm.c</tt> file, but this isn't
really usable for the Thumb-2 ports. Instead, the <tt>fpithumb2.c</tt> file
contains the trickery needed to support the instructions that these MCUs don't
provide. I previously covered <a href="2023-09-25.html">some of the issues</a>
that make the implementation of the emulator a bit different.
</p>

<p>
It seems that the STM32F405 MCU supports slightly fewer instructions than the
Apollo 3 MCU, so the <tt>fpithumb2.c</tt> implementation needed tweaking to
reflect this. Fortunately, I was a bit overenthusiastic when writing the
original implementation, so those extra unsupported instructions were quickly
supported by uncommenting some code.
</p>

<h3>Exception handling</h3>

<p>
Since the two ports are both running Thumb-2 code on Cortex-M4-based MCUs,
there is a certain amount of code that is common between them, but which isn't
more widely portable, partly because there is some assembly language.
As a result, the code lives in the directories for those ports.
</p>

<p>
When an undefined instruction occurs, the MCU is configured to raise a usage
fault exception. The address for the usage fault handler in the vector table is
used to locate the handler itself. The handler is implemented in the <tt>l.s</tt>
file. Since we're going to be doing things with floating point registers, it's
probably a good idea to save them along with the regular registers, and the
MCU can be configured to do this automatically when an exception occurs.
This is done in the <tt>trap.c</tt> file.
</p>

<p>
Since floating point registers are saved, the structures that let C code
access the stack needed to be updated, and I introduced another structure in
the <tt>include/ureg.h</tt> file for this because different handlers end up
stacking registers in different ways.
</p>

<h2>Priorities</h2>

<p>
Previously, there wasn't much other than the SysTick exception that could
interrupt running code. However, with undefined floating point instructions now
causing usage fault exceptions alongside the periodic SysTick, some
organisation is now needed. This is done by giving the SysTick exception a
lower priority than the other exceptions, although this is done by assigning it
a higher priority number. This prevents the potential issue that either of the
exceptions could occur when the other is already active, causing a hard fault.
</p>

<p>
Because the interrupt priority handling seemed insufficient in some cases while
I was working on floating point emulation, I also introduced safeguards to the
SysTick handler to return if another exception was already being handled, but I
think that these are redundant, so I removed them.
</p>

<h2>Interrupt handling</h2>

<p>
One problem that occurs when supporting undefined floating point instructions
is that interrupts need to be enabled for the appropriate usage fault exception
to be delivered. If interrupts are disabled, a hard fault occurs. This required
the implementations of the <tt>splhi</tt>, <tt>spllo</tt>, <tt>splx</tt> and
<tt>islo</tt> functions to be modified to work without actually disabling
interrupts. Basically, instead of disabling interrupts completely, these
functions are used to mask, unmask and handle the state of the SysTick
exception and potentially other low-priority exceptions.
</p>

<p>
Because the earlier way of enabling and disabling interrupts worked for the
initial ports, I kept it around while experimenting, but the STM32F405 port
discards it completely.
</p>

<h2>Tests</h2>

<p>
There's not much in the way of tests for the floating point support. I added
<tt>appl/floattest.b</tt> and <tt>appl/textmandel.b</tt> to both the ports.
The first of these runs through some basic arithmetic and comparison tests.
The second is a visual test to make sure that things look reasonable, and it's
a bit of a reward for having put in the effort.
</p>

<h2>Downloads</h2>

<p>
My <a href="https://github.com/dboddie/inferno-os">Inferno fork</a> has
branches for the ports to different MCUs. The <tt>apollo3-fp</tt> and
<tt>stm32f405-fp</tt> branches contain the changes mentioned above.
</p>

<p>
Builds for STM32F405 and Apollo3 that include floating point support can be
obtained from the <a href="https://github.com/dboddie/inferno-test-builds">Inferno
Test Builds</a> repository.
</p>
<!-- /content -->

<!-- name -->David Boddie<!-- /name -->
<!-- date -->27 October 2023<!-- /date -->
