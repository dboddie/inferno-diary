<!-- title -->Blinking an LED<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- content -->

<p>
With a <a href="2022-01-30.html">compiler and build script in place</a>, it's
possible to build something to deploy to the microcontroller. The first test is
the traditional blinking LED example, obtained from
<a href="https://gitlab.com/dboddie/bare-metal-stm32f405">this git repository</a>.
</p>

<h2>The main program</h2>

<p>
The <tt>main.c</tt> file contains code to set up a timer and toggle power to
the status LED on the <a href="https://www.sparkfun.com/products/17713">MicroMod
STM32 Processor</a> board.
</p>

<p>
We start by including definitions for the structures and memory locations used
to access the board's hardware:
</p>

<div class="highlight"><pre><span class="cp">#include</span> <span class="cpf">&quot;../stm32f405.h&quot;</span><span class="cp"></span>
</pre></div>

<p>
The test uses the system timer to determine when to change the state of the LED,
so we create a function to set this up, overlaying a structure onto a region of
memory where the timer registers are stored:
</p>

<div class="highlight"><pre><span class="kt">void</span> <span class="nf">start_timer</span><span class="p">(</span><span class="kt">void</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">SysTick</span> <span class="o">*</span><span class="n">tick</span> <span class="o">=</span> <span class="p">(</span><span class="n">SysTick</span> <span class="o">*</span><span class="p">)</span><span class="n">SYSTICK</span><span class="p">;</span>
    <span class="cm">/* The system clock is 16MHz, so set a reset value for 1s. */</span>
    <span class="n">tick</span><span class="o">-&gt;</span><span class="n">reload</span> <span class="o">=</span> <span class="mi">16000000</span> <span class="o">-</span> <span class="mi">1</span><span class="p">;</span>
    <span class="n">tick</span><span class="o">-&gt;</span><span class="n">current</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span>
    <span class="n">tick</span><span class="o">-&gt;</span><span class="n">control</span> <span class="o">=</span> <span class="mi">5</span><span class="p">;</span>  <span class="cm">/* 4=processor clock, 2=SysTick exception, 1=enable */</span>
<span class="p">}</span>
</pre></div>

<p>
Note that we don't enable the exception that the timer can raise when it wraps
around. In this example we will poll the timer's state instead of using
interrupts.
</p>

<p>
The <tt>systick</tt> function checks a bit in the timer's control register to
determine whether it has wrapped around. If so, it toggles the state of the LED:
</p>

<div class="highlight"><pre><span class="kt">void</span> <span class="nf">systick</span><span class="p">(</span><span class="kt">void</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">SysTick</span> <span class="o">*</span><span class="n">tick</span> <span class="o">=</span> <span class="p">(</span><span class="n">SysTick</span> <span class="o">*</span><span class="p">)</span><span class="n">SYSTICK</span><span class="p">;</span>
    <span class="n">GPIO</span> <span class="o">*</span><span class="n">gpioa</span> <span class="o">=</span> <span class="p">(</span><span class="n">GPIO</span> <span class="o">*</span><span class="p">)</span><span class="n">GPIO_A</span><span class="p">;</span>

    <span class="k">if</span> <span class="p">(</span><span class="n">tick</span><span class="o">-&gt;</span><span class="n">control</span> <span class="o">&amp;</span> <span class="mh">0x10000</span><span class="p">)</span>
        <span class="n">gpioa</span><span class="o">-&gt;</span><span class="n">odr</span> <span class="o">^=</span> <span class="mi">1</span> <span class="o">&lt;&lt;</span> <span class="mi">15</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>

<p>
The <tt>main</tt> function enables GPIOs on one of the buses by setting a bit
in a control register. This is hidden in a function in other tests. It then
continues by configuring the GPIO line used to access the status LED:
</p>

<div class="highlight"><pre><span class="cp">#define RCC_AHB1_ENABLE 0x40023830</span>

<span class="kt">void</span> <span class="nf">main</span><span class="p">(</span><span class="kt">void</span><span class="p">)</span>
<span class="p">{</span>
    <span class="cm">/* The GPIO pins are on the AHB bus and must be enabled */</span>
    <span class="o">*</span><span class="p">(</span><span class="n">uint</span> <span class="o">*</span><span class="p">)</span><span class="n">RCC_AHB1_ENABLE</span> <span class="o">|=</span> <span class="n">RCC_AHB1_ENABLE_GPIO_A</span><span class="p">;</span>

    <span class="n">GPIO</span> <span class="o">*</span><span class="n">gpioa</span> <span class="o">=</span> <span class="p">(</span><span class="n">GPIO</span> <span class="o">*</span><span class="p">)</span><span class="n">GPIO_A</span><span class="p">;</span>
    <span class="cm">/* Set the pin mode using the pair of bits for PA15: */</span>
    <span class="n">gpioa</span><span class="o">-&gt;</span><span class="n">moder</span> <span class="o">=</span> <span class="n">GPIO_Output</span> <span class="o">&lt;&lt;</span> <span class="mi">30</span><span class="p">;</span>
    <span class="cm">/* Set the speed */</span>
    <span class="n">gpioa</span><span class="o">-&gt;</span><span class="n">ospeedr</span> <span class="o">=</span> <span class="n">GPIO_HighSpeed</span> <span class="o">&lt;&lt;</span> <span class="mi">30</span><span class="p">;</span>
    <span class="cm">/* Turn on the status LED */</span>
    <span class="n">gpioa</span><span class="o">-&gt;</span><span class="n">odr</span> <span class="o">=</span> <span class="mi">1</span> <span class="o">&lt;&lt;</span> <span class="mi">15</span><span class="p">;</span>

    <span class="n">start_timer</span><span class="p">();</span>

    <span class="k">for</span> <span class="p">(;;)</span> <span class="p">{</span>
        <span class="n">systick</span><span class="p">();</span>
    <span class="p">}</span>
<span class="p">}</span>
</pre></div>

<p>
Finally, it calls the <tt>start_timer</tt> function and repeatedly calls the
<tt>systick</tt> function to poll for timer wraparound, toggling the LED state
when that occurs.
</p>

<p>
It's not a very elegant way to toggle the LED in response to a timer, but it's
fairly simple to implement and test.
</p>

<h2>Building and flashing the code</h2>

<p>
As in the previous article, building the code should be a simple matter of
entering the <tt>blink_systick</tt> directory and typing <tt>mk</tt> to produce
a file called <tt>out.bin</tt> in the current directory.
</p>

<p>
Vendors of STM32 MCUs would probably recommend that you use an SDK to flash
software to the processor.
<a href="https://learn.sparkfun.com/tutorials/micromod-stm32-processor-hookup-guide">SparkFun
says</a> that you need to install the STM32Cube software, but it's not
necessary. I use the <a href="https://sourceforge.net/projects/stm32flash/">stm32flash</a>
tool that I install from the Debian package repository.
</p>

<p>
Assuming that the processor is connected to your computer via USB and it has
been put into programming mode, find out which device file it appears as.
In the directory containing the <tt>out.bin</tt> file, run <tt>stm32flash</tt>
with options to write the file and reset the processor. For me, the board with
the processor appears as the <tt>/dev/ttyUSB0</tt> file, so I use the following
command:
</p>

<pre>
stm32flash -R -w out.bin /dev/ttyUSB0
</pre>

<p>
The output looks like this:
</p>

<pre>
stm32flash 0.5

http://stm32flash.sourceforge.net/

Using Parser : Raw BINARY
Interface serial_posix: 57600 8E1
Version      : 0x31
Option 1     : 0x00
Option 2     : 0x00
Device ID    : 0x0413 (STM32F40xxx/41xxx)
- RAM        : 128KiB  (12288b reserved by bootloader)
- Flash      : 1024KiB (size first sector: 1x16384)
- Option RAM : 16b
- System RAM : 30KiB
Write to memory
Erasing memory
Wrote address 0x080002c8 (100.00%) Done.

Resetting device... done.
</pre>

<p>
At this point, the status LED should change state once every second or so.
</p>

<h2>Going further</h2>

<p>
The example code shown above is fairly simple and doesn't use interrupts to
help schedule tasks, set up clocks for other peripherals or change the
processor speed. It would also be good to set up a UART so that the processor
can send debugging messages for the connected computer to display.
</p>
<!-- /content -->

<!-- name -->David Boddie<!-- /name -->
<!-- date -->30 January 2022<!-- /date -->
