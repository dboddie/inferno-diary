<!-- title -->E-Ink Display<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- content -->

<p>
I've been interested in colour e-ink displays for a while, but couldn't justify
the expense purely for the purpose of tinkering. During the summer last year, I
picked up an <a href="https://shop.pimoroni.com/products/inky-impression-4?variant=39599238807635">Inky
Impression</a> display while it was on sale at Pimoroni. This gave me a chance
to experiment with one, and I was surprised how good it turned out to be.
</p>

<h2>First impressions</h2>

<p>
The display arrived well-packaged in a padded bag inside a sturdy cardboard
case. One particularly nice touch by the vendor was the use of a decorative
image on the display, which showcases the sort of pictures that work well.
Given that the palette only contains 7 colours, and photographic images need to
be processed to use these effectively, the results can be very impressive.
It helps that the resolution of 640 by 400 pixels is high enough to support
effective use of dithering.
</p>

<div class="centre"><img class="dark" src="images/2023-02-27-inky0.jpg" /><br />
<span class="caption">The display with its default image.</span>
</div>

<p>
At this point during the year I was doing things with bare metal code on the
STM32 MicroMod processor card and I started working on some code to send data
to the display. This took advantage of the existing code in Pimoroni's
<a href="https://github.com/pimoroni/inky">Inky repository</a>, but was
rewritten in the language I was using to experiment with embedded hardware.
</p>

<p>
The display is programmed via a Serial Peripheral Interface (SPI) bus, which
the STM32 makes fairly simple once the hardware is set up according to the
datasheet. Of course, the target market of the display will be using either
Python (MicroPython or CircuitPython) or C++ (in the Arduino environment) so
this is almost at the level of plug-and-play for experienced makers. Still,
with a bit of experience, even someone writing their own technology stack from
the compiler up can eventually send image data to the display.
</p>

<div class="centre"><img class="dark" src="images/2023-02-27-inky1.jpg" /><br />
<span class="caption">An image of <a href="https://en.wikipedia.org/wiki/Trakai">Trakai</a>
Castle.</span>
</div>

<p>
None of this is very Inferno-related. However, once some level of stability was
achieved with the port to the STM32 processor board, something could be done to
make the display available to Inferno and Limbo programs.
</p>

<h2>An e-ink device</h2>

<p>
Ideally, we would expose the SPI bus as a file system to the Inferno
environment and write Limbo programs to interact with it. However, the code to
access the display was already written in a low-level language, so it seemed
easier to just expose two files and let the device driver handle the SPI bus.
The file system provides a <tt>data</tt> file that accepts pixel data until
enough has been received to send to the display, and a <tt>status</tt> file
that can be read to obtain very simple information about the status of the
display. The init script binds the device to the <tt>/dev/ink</tt> directory:
</p>

<pre>
stm32f405$ ls /dev/ink
data
status
</pre>

<p>
Generally, only the <tt>data</tt> file is useful, and it can be written to
using the usual commands, assuming that there is enough memory left to use
them if image data is in the root file system taking up RAM. Here, image data
for a quarter of the screen is sent to the display four times:
</p>

<pre>
cat /tmp/piece /tmp/piece /tmp/piece /tmp/piece > /dev/ink/data
</pre>

<p>
A more practical way is to use a Limbo program to generate data and use
redirection to send it to the e-ink device.
</p>

<h2>MakeColour</h2>

<p>
This is what a program to generate a series of coloured stripes looks like.
It starts with the usual declarations, includes and definitions:
</p>

<div class="highlight"><pre><span></span><span class="k">implement</span> <span class="n">MakeColour</span><span class="p">;</span>

<span class="k">include</span> <span class="s">&quot;draw.m&quot;</span><span class="p">;</span>
<span class="k">include</span> <span class="s">&quot;sys.m&quot;</span><span class="p">;</span>

<span class="n">BUFSIZE</span><span class="o">:</span> <span class="kc">con</span> <span class="mi">160</span><span class="p">;</span>

<span class="n">MakeColour</span><span class="o">:</span> <span class="kt">module</span>
<span class="p">{</span>
    <span class="n">init</span><span class="o">:</span>   <span class="kt">fn</span><span class="p">(</span><span class="kc">nil</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">,</span> <span class="n">args</span><span class="o">:</span> <span class="kt">list</span> <span class="kt">of</span> <span class="kt">string</span><span class="p">);</span>
<span class="p">};</span>
</pre></div>

<p>
The <tt>BUFSIZE</tt> is set to a reasonably low value so that the program's
memory footprint is kept low. Each line of pixels on the screen is 320 bytes
because each pixel is encoded in 4 bits, with 1 bit unused, so only half a line
of data is sent at once.
</p>

<p>
The <tt>init</tt> function starts with some fairly standard checks:
</p>

<div class="highlight"><pre><span class="n">init</span><span class="p">(</span><span class="kc">nil</span><span class="o">:</span> <span class="kt">ref</span> <span class="n">Draw</span><span class="o">-&gt;</span><span class="n">Context</span><span class="p">,</span> <span class="n">args</span><span class="o">:</span> <span class="kt">list</span> <span class="kt">of</span> <span class="kt">string</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">sys</span> <span class="o">:=</span> <span class="k">load</span> <span class="n">Sys</span> <span class="n">Sys</span><span class="o">-&gt;</span><span class="n">PATH</span><span class="p">;</span>
    <span class="n">stdout</span> <span class="o">:=</span> <span class="n">sys</span><span class="o">-&gt;</span><span class="n">fildes</span><span class="p">(</span><span class="mi">1</span><span class="p">);</span>

    <span class="k">if</span> <span class="p">(</span><span class="k">len</span> <span class="n">args</span> <span class="o">!=</span> <span class="mi">1</span><span class="p">)</span> <span class="p">{</span>
        <span class="n">sys</span><span class="o">-&gt;</span><span class="n">fprint</span><span class="p">(</span><span class="n">sys</span><span class="o">-&gt;</span><span class="n">fildes</span><span class="p">(</span><span class="mi">2</span><span class="p">),</span> <span class="s">&quot;usage: makecol</span><span class="se">\n</span><span class="s">&quot;</span><span class="p">);</span>
        <span class="k">return</span><span class="p">;</span>
    <span class="p">}</span>
</pre></div>

<p>
We then set up an array of bytes that will be used to prepare data for the
display, then we start a loop to paint a stripe of each colour:
</p>

<div class="highlight"><pre>
    <span class="n">b</span> <span class="o">:=</span> <span class="kt">array</span><span class="p">[</span><span class="n">BUFSIZE</span><span class="p">]</span> <span class="kt">of</span> <span class="kt">byte</span><span class="p">;</span>
    <span class="n">c</span> <span class="o">:=</span> <span class="mi">0</span><span class="p">;</span>

    <span class="n">for</span> <span class="p">(</span><span class="n">i</span> <span class="o">:=</span> <span class="mi">0</span><span class="p">;</span> <span class="n">i</span> <span class="o">&lt;</span> <span class="mi">8</span><span class="p">;</span> <span class="n">i</span><span class="o">++</span><span class="p">)</span> <span class="p">{</span>
        <span class="n">cb</span> <span class="o">:=</span> <span class="kt">byte</span> <span class="n">c</span><span class="p">;</span>
        <span class="n">for</span> <span class="p">(</span><span class="n">j</span> <span class="o">:=</span> <span class="mi">0</span><span class="p">;</span> <span class="n">j</span> <span class="o">&lt;</span> <span class="n">BUFSIZE</span><span class="p">;</span> <span class="n">j</span><span class="o">++</span><span class="p">)</span>
            <span class="n">b</span><span class="p">[</span><span class="n">j</span><span class="p">]</span> <span class="o">=</span> <span class="n">cb</span> <span class="o">|</span> <span class="p">(</span><span class="n">cb</span> <span class="o">&lt;&lt;</span> <span class="mi">4</span><span class="p">);</span>

        <span class="n">for</span> <span class="p">(</span><span class="n">j</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="n">j</span> <span class="o">&lt;</span> <span class="mi">100</span><span class="p">;</span> <span class="n">j</span><span class="o">++</span><span class="p">)</span>
            <span class="k">if</span> <span class="p">(</span><span class="n">sys</span><span class="o">-&gt;</span><span class="n">write</span><span class="p">(</span><span class="n">stdout</span><span class="p">,</span> <span class="n">b</span><span class="p">,</span> <span class="n">BUFSIZE</span><span class="p">)</span> <span class="o">!=</span> <span class="n">BUFSIZE</span><span class="p">)</span> <span class="k">return</span><span class="p">;</span>

        <span class="n">c</span> <span class="o">=</span> <span class="p">(</span><span class="n">c</span> <span class="o">+</span> <span class="mi">1</span><span class="p">)</span> <span class="o">%</span> <span class="mi">7</span><span class="p">;</span>
    <span class="p">}</span>
<span class="p">}</span>
</pre></div>

<p>
The first inner loop fills the buffer with data for each colour. The second
loop writes the contents of the buffer to stdout enough times to fill an eighth
of the display. Since the palette only contains 7 colours, the first colour
appears twice.
</p>

<p>
When the program is run, we use redirection to send the output to the e-ink
device:
</p>

<pre>
makecol > /dev/ink/data
</pre>

<div class="centre"><img class="dark" src="images/2023-03-04-inky2.jpg" /><br />
<span class="caption">Some colourful stripes.</span>
</div>

<p>
We can use this method to write text to the display, though text output isn't
really the best use of its capabilities and it's not suitable for interactive
use. I've written something based on the <tt>MakeColour</tt> program that sends
character bitmaps to the display, but it's not very interesting to look at.
It's possible to write some text to the display with a command like this:
</p>

<tt>
ls dis | mc -c 40 | ink > /dev/ink/data
</tt>

<p>
It could be used to display information or a calendar, but maybe a cheaper,
lower resolution, monochrome e-ink display would be more suitable for those
purposes.
</p>
<!-- /content -->

<!-- name -->David Boddie<!-- /name -->
<!-- date -->4 March 2023<!-- /date -->
