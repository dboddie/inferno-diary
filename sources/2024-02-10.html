<!-- title -->Debugging and memfs<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- summary -->
Using simple tools to help debug floating point register handling code.
<!-- /summary -->

<!-- content -->
<p>
One thing I noticed when <a href="2023-10-14.html">testing floating point
handling code</a> was that running the <tt>textmandel</tt> test program in the
foreground while also running it in the background would result in the
foreground task producing invalid output. This is exactly the sort of thing I
was worried would happen, and I spent some time thinking about it and putting
off doing something about it.
</p>

<h2>Hypotheses and approaches</h2>

<p>
I had some ideas about what might be happening, but I needed to formulate ways
to test my hypotheses. Even before jumping in to try and debug the problem, it
was worth spending some time to verify that what I was seeing was in fact the
problem I thought it was: something to do with the floating point registers.
</p>

<p>
So, the first task was to change the <tt>textmandel</tt> program to produce
plain ASCII output, just to see whether its lack of output when run as both
a foreground and background task was a display issue. Sure enough, I still got
a series of blank lines as output to the foreground task, but what about the
background task? Up to this point I had been sending the output of the
background task to the null device:
</p>

<pre>
apollo3$ textmandel > /dev/null &
</pre>

<p>
With the program now outputting reasonably loggable text, I needed to check
that the output from the background task was correct. The problem is that I
haven't yet implemented the driver for the microSD card that one of the
MicroMod boards provides.
</p>

<h2>Using memfs</h2>

<p>
Fortunately, Inferno has a memory file system called <tt>memfs</tt> that we can
use in situations like these. Even more fortunately, it doesn't need the
<tt>mount</tt> tool to mount it, because that has quite a few dependencies.
The way to run it is also very simple:
</p>

<pre>
apollo3$ memfs
</pre>

<p>
This starts serving on <tt>/tmp</tt> by default, giving us a place to store
temporary files in RAM. Since we don't have a lot of RAM to spare, a lot of the
Limbo modules <a href="2023-06-28.html">needed to be frozen</a> to reduce their
run-time overhead. The result was that the test could be run like this:
</p>

<pre>
apollo3$ textmandel > /tmp/tm &
apollo3$ textmandel
</pre>

<p>
After both of these had finished running, the <tt>/tmp/tm</tt> file could be
inspected, showing that the output was correct. A further test could be run
to check that the problem wasn't something wrong with the console output:
</p>

<pre>
apollo3$ textmandel > /tmp/t1 &
apollo3$ textmandel > /tmp/t2 &
apollo3$ textmandel
</pre>

<p>
In this case, only <tt>/tmp/t1</tt> contained correct output.
</p>

<h2>Narrowing down causes</h2>

<p>
The two places where problems could potentially lurk are the task switching
routine and the floating point emulation code. For a while, I tried writing
out the contents of the floating point registers when task switching occurred.
This didn't really reveal any obvious problems, but it was difficult to know
if the values in the registers were correct, even if they weren't obviously
incorrect.
</p>

<p>
I modified the <tt>textmandel</tt> program to print numerical values instead of
symbols or colours, then ran it again in the same way as before. This time it
was clear that the floating point values in the second, foreground task were
incorrect, with only <tt>Inf</tt> and <tt>Nan</tt> being printed.
</p>

<p>
Having seen the code to a few different ports of Inferno, I remembered that
there are various functions to save and restore the floating point state.
However, these are often left unimplemented, and this was the case in my code.
So, I wrote what I thought was reasonable code for the <tt>FPsave</tt> and
<tt>FPrestore</tt> functions, only to discover that it had no effect.
</p>

<p>
I looked at the floating point emulator code which I had adapted from the
32-bit ARM ports. In particular, the main <tt>fpithumb2</tt> function that
handles each undefined instruction contains code like this:
</p>

<div class="highlight"><pre><span></span><span class="n">ufp</span> <span class="o">=</span> <span class="o">&amp;</span><span class="n">up</span><span class="o">-&gt;</span><span class="n">env</span><span class="o">-&gt;</span><span class="n">fpu</span><span class="p">;</span>
<span class="k">if</span><span class="p">(</span><span class="n">ufp</span><span class="o">-&gt;</span><span class="n">fpistate</span> <span class="o">!=</span> <span class="n">FPACTIVE</span><span class="p">)</span> <span class="p">{</span>
    <span class="n">ufp</span><span class="o">-&gt;</span><span class="n">fpistate</span> <span class="o">=</span> <span class="n">FPACTIVE</span><span class="p">;</span>
    <span class="n">ufp</span><span class="o">-&gt;</span><span class="n">control</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span>
    <span class="n">ufp</span><span class="o">-&gt;</span><span class="n">status</span> <span class="o">=</span> <span class="p">(</span><span class="mh">0x01</span><span class="o">&lt;&lt;</span><span class="mi">28</span><span class="p">)</span><span class="o">|</span><span class="p">(</span><span class="mi">1</span><span class="o">&lt;&lt;</span><span class="mi">12</span><span class="p">);</span>   <span class="cm">/* software emulation, alternative C flag */</span>
    <span class="k">for</span> <span class="p">(</span><span class="kt">int</span> <span class="n">n</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="n">n</span> <span class="o">&lt;</span> <span class="mi">16</span><span class="p">;</span> <span class="n">n</span><span class="o">++</span><span class="p">)</span>
        <span class="n">er</span><span class="o">-&gt;</span><span class="n">s</span><span class="p">[</span><span class="n">n</span><span class="p">]</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>

<p>
Looking at this again, I realised that I'd never looked at what the different
members of the <tt>FPenv</tt> struct were being used for. A search in the
<tt>os/port</tt> directory, in my own ports, and in various other ports
indicated that much of this code had no effect at all. The only part of it that
could have an effect is the initialisation of the registers via locations in
the stack frame, so I commented it out to see what effect it would have.
The result was that both the foreground and background tasks produced the
correct output.
</p>

<h2>Why does it work?</h2>

<p>
I think the key difference between my adapted emulation code and the earlier
code it is based on is its use of live floating point registers. A lot of the
earlier ARM ports are doing full emulation of floating point instructions and,
as a result, they maintain a virtual set of registers. I don't need to do that,
and I certainly shouldn't need to initialise the stacked floating point
registers.
</p>

<p>
This experience has made me a bit more careful about code I reuse from other
ports, which is hopefully a good thing. Not that the code is bad, but perhaps
not quite as immediately applicable as I thought it was.
</p>

<!-- /content -->
12345678901234567890123456789012345678901234567890123456789012345678901234567890
<!-- name -->David Boddie<!-- /name -->
<!-- date -->10 February 2024<!-- /date -->
