<!-- title -->More Thumb Compiler Notes<!-- /title -->
<!-- template -->simple.html<!-- /template -->

<!-- content -->

<p>
In <a href="2022-01-29.html">my previous entry</a> I provided a bit of
information about compiling Thumb code with the <tt>5a</tt>, <tt>tc</tt> and
<tt>tl</tt> tools provided with Inferno. I'll try to add more practical
information so that you can actually build and run something on real hardware.
</p>

<h2>Resources</h2>

<p>
I put some tests and library code in a
<a href="https://gitlab.com/dboddie/bare-metal-stm32f405">git repository</a> so
that I could refer to things that people can access.
</p>

<h2>Build scripts</h2>

<p>
Initially, I just used Python scripts to build the tests. Just before pushing
the repository to GitLab, I wrote a <tt>mkfile</tt> for each of them, with one
at the top level to build them all at once. The first key part of each
<tt>mkfile</tt> are the definitions for the object file suffix, tools and flags:
</p>

<div class="highlight"><pre><span class="nv">O</span><span class="o">=</span>          t
<span class="nv">AS</span><span class="o">=</span>         5a
<span class="nv">ASFLAGS</span><span class="o">=</span>    -t
<span class="nv">CC</span><span class="o">=</span>         tc
<span class="nv">CFLAGS</span><span class="o">=</span>     -wFV
<span class="nv">LD</span><span class="o">=</span>         tl
<span class="nv">LDFLAGS</span><span class="o">=</span>    -l -s -R0 -H0 -T0x08000000 -D0x20000000
</pre></div>

<p>
The <tt>O</tt> variable holds the file suffix for object files. <tt>AS</tt>,
<tt>CC</tt> and <tt>LD</tt> are the assembler, compiler and linker to use.
The <tt>ASFLAGS</tt> contains only the <tt>-t</tt> option which tells the
assembler to generate Thumb code. The compiler flags are generic.
</p>

<p>
The <tt>LDFLAGS</tt> variable contains a few options that can be helpfully
explained:
</p>

<ul>
<li><tt>-l</tt> stops the linker trying to find the <tt>_main</tt> symbol.
It might be better to include this in the future and define <tt>_main</tt>.</li>
<li><tt>-s</tt> strips the symbol table from the output.</li>
<li><tt>-H0</tt> selects a headerless output format because the code will be
executed directly, not loaded as an executable within an operating system.</li>
<li><tt>-T0x08000000</tt> tells the linker to build code that loads and runs
at the address 0x08000000, which is the start of flash memory in the
microcontroller.</li>
<li><tt>-D0x20000000</tt> tells the linker that data used by the program should
be located at 0x20000000, which is in RAM. The data doesn't magically appear in
RAM, but this is something we deal with later.</li>
<li><tt>-R0</tt> tells the linker that it the text (code) and data sections are
separate. This causes the linker to use data addresses based on the address
passed with the <tt>-D</tt> option.</li>
</ul>

<p>
Skipping some rules that define how to assemble and compile files of different
types, the files to be built are defined using the <tt>OBJ</tt> variable.
These are the <tt>main.c</tt> file in the current directory and the <tt>l.s</tt>
and <tt>div.s</tt> files in the parent directory:
</p>

<div class="highlight"><pre><span class="cm"># Order is important - the loader must be first:</span>
<span class="nv">OBJ</span><span class="o">=</span><span class="se">\</span>
	../l.<span class="nv">$O</span><span class="se">\</span>
	main.<span class="nv">$O</span><span class="se">\</span>
	../div.<span class="nv">$O</span><span class="se">\</span>

<span class="nv">NOPREFIXOBJ</span><span class="o">=</span><span class="si">${</span><span class="nv">OBJ</span><span class="p">:../%=%</span><span class="si">}</span>

<span class="nv">BIN</span><span class="o">=</span>out.bin
</pre></div>

<p>
The <tt>NOPREFIXOBJ</tt> variable contains a rule to strip the leading path to
the parent directory from the object files, and the <tt>BIN</tt> variable
defines the name of the file containing the executable code. Ignoring other
generic rules, the last key rule defines how the object files are linked:
</p>

<div class="highlight"><pre><span class="nf">$BIN</span><span class="o">:</span>	$<span class="n">OBJ</span>
	<span class="nv">$LD</span> <span class="nv">$LDFLAGS</span> -o <span class="nv">$BIN</span> <span class="nv">$NOPREFIXOBJ</span>
	python3 ../fix_vector_table.py <span class="nv">$BIN</span> 0x188
</pre></div>

<p>
Here, <tt>$BIN</tt> is dereferenced to <tt>out.bin</tt>, which depends on the
files in the <tt>OBJ</tt> list being built. The linker is invoked on the
processed list of files in <tt>NOPREFIXOBJ</tt>. The last thing we do is run a
Python script to process the executable file. This is explained below.
</p>

<h2>The vector table</h2>

<p>
The executable we build isn't just code. The
<a href="https://gitlab.com/dboddie/bare-metal-stm32f405/-/blob/master/l.s">loader
code in <tt>l.s</tt></a> starts with the following lines:
</p>

<div class="highlight"><pre><span></span><span class="cp">#include</span> <span class="cpf">&quot;thumb2.h&quot;</span><span class="cp"></span>
<span class="cp">#include</span> <span class="cpf">&quot;vectors.s&quot;</span><span class="cp"></span>
</pre></div>

<p>
The <a href="https://gitlab.com/dboddie/bare-metal-stm32f405/-/blob/master/vectors.s">contents
of <tt>vectors.s</tt></a> is a definition containing a sequence of function
addresses. This is the vector table for the processor which tells it where to
jump to when it is reset, receives interrupts or has to process exceptions and
traps. This is the start of it:
</p>

<div class="highlight"><pre><span class="cp">#define STACKTOP 0x20020000</span>
<span class="cp">#define DUMMY _dummy(SB)</span>
<span class="cp">#define RESET _start(SB)</span>
<span class="cp">#define SYSTICK _systick(SB)</span>

<span class="cm">/* See page 372 of the STM32F405/415 Reference Manual RM0090 */</span>

<span class="n">TEXT</span> <span class="n">vectors</span><span class="p">(</span><span class="n">SB</span><span class="p">),</span> <span class="err">$</span><span class="mi">0</span>
    <span class="n">WORD</span>    <span class="err">$</span><span class="n">STACKTOP</span>
    <span class="n">WORD</span>    <span class="err">$</span><span class="n">RESET</span>
    <span class="n">WORD</span>    <span class="err">$</span><span class="n">DUMMY</span>
    <span class="n">WORD</span>    <span class="err">$</span><span class="n">DUMMY</span>
    <span class="n">WORD</span>    <span class="err">$</span><span class="n">DUMMY</span>
    <span class="n">WORD</span>    <span class="err">$</span><span class="n">DUMMY</span>
    <span class="n">WORD</span>    <span class="err">$</span><span class="n">DUMMY</span>
</pre></div>

<p>
As a result, the executable contains the processor's vector table followed by
routines that the table references. Our main code is run from the routine that
is executed when a reset occurs. This is found in the <tt>l.s</tt> file.
</p>

<p>
Although the linker outputs all the correct addresses for the routines in the
table, unfortunately that's not enough for the processor. Because the code is
Thumb code, bit 0 of each valid entry needs to be set. Ideally, this would be
done in the <tt>vectors.s</tt> file, but I couldn't work out what the syntax
would be to encode the correct values. As a result, the <tt>fix_vector_table.py</tt>
script performs this task, updating the entries where necessary.
</p>

<h2>The loader</h2>

<p>
Before the C code can be executed, some preparation is needed: various
registers need to be set up and mutable data needs to be copied into RAM. This
is done by the loader, which is provided by the
<a href="https://gitlab.com/dboddie/bare-metal-stm32f405/-/blob/master/l.s">assembly
language in the <tt>l.s</tt> file</a>. As noted before, it starts by including
some definitions, including the vector table:
</p>

<div class="highlight"><pre><span class="cp">#include</span> <span class="cpf">&quot;thumb2.h&quot;</span><span class="cp"></span>
<span class="cp">#include</span> <span class="cpf">&quot;vectors.s&quot;</span><span class="cp"></span>

<span class="nf">THUMB</span><span class="err">=</span><span class="mi">4</span>
<span class="nf">STACK_TOP</span><span class="err">=</span><span class="mi">0x20020000</span>
</pre></div>

<p>
The <tt>_start</tt> routine, which is called when a reset occurs, is responsible
for setting up the environment for the C code. It begins by setting the static
base and stack pointer registers. The static base register is used to refer to
constant and mutable data that the program uses, such as string constants and
global variables, but also other values that cannot easily be expressed using
the limited encodings allowed by the instruction set.
</p>

<div class="highlight"><pre><span class="nf">TEXT</span> <span class="no">_start</span><span class="p">(</span><span class="no">SB</span><span class="p">),</span> <span class="no">THUMB</span><span class="p">,</span> <span class="no">$-4</span>

    <span class="nf">MOVW</span>    <span class="no">$setR12</span><span class="p">(</span><span class="no">SB</span><span class="p">),</span> <span class="no">R1</span>
    <span class="nf">MOVW</span>    <span class="no">R1</span><span class="p">,</span> <span class="no">R12</span>	<span class="err">/</span><span class="p">*</span> <span class="no">static</span> <span class="no">base</span> <span class="p">(</span><span class="no">SB</span><span class="p">)</span> <span class="p">*</span><span class="err">/</span>
    <span class="nf">MOVW</span>    <span class="no">$STACK_TOP</span><span class="p">,</span> <span class="no">R1</span>
    <span class="nf">MOVW</span>    <span class="no">R1</span><span class="p">,</span> <span class="no">SP</span>
</pre></div>

<p>
The next part of the routine copies the data from flash memory into RAM, using
the symbols <tt>etext</tt>, <tt>bdata</tt> and <tt>edata</tt> that the linker
defines internally:
</p>

<div class="highlight"><pre>
    <span class="cm">/* Copy initial values of data from after the end of the text section to</span>
<span class="cm">       the beginning of the data section. */</span>
    <span class="nf">MOVW</span>    <span class="no">$etext</span><span class="p">(</span><span class="no">SB</span><span class="p">),</span> <span class="no">R1</span>
    <span class="nf">MOVW</span>    <span class="no">$bdata</span><span class="p">(</span><span class="no">SB</span><span class="p">),</span> <span class="no">R2</span>
    <span class="nf">MOVW</span>    <span class="no">$edata</span><span class="p">(</span><span class="no">SB</span><span class="p">),</span> <span class="no">R3</span>

<span class="nl">_start_loop:</span>
    <span class="nf">CMP</span>     <span class="no">R3</span><span class="p">,</span> <span class="no">R2</span>              <span class="cm">/* Note the reversal of the operands */</span>
    <span class="nf">BGE</span>     <span class="no">_end_start_loop</span>

    <span class="nf">MOVW</span>    <span class="p">(</span><span class="no">R1</span><span class="p">),</span> <span class="no">R4</span>
    <span class="nf">MOVW</span>    <span class="no">R4</span><span class="p">,</span> <span class="p">(</span><span class="no">R2</span><span class="p">)</span>
    <span class="nf">ADD</span>     <span class="no">$4</span><span class="p">,</span> <span class="no">R1</span>
    <span class="nf">ADD</span>     <span class="no">$4</span><span class="p">,</span> <span class="no">R2</span>
    <span class="nf">B</span>       <span class="no">_start_loop</span>

<span class="nl">_end_start_loop:</span>

    <span class="nf">B</span>   <span class="p">,</span><span class="no">main</span><span class="p">(</span><span class="no">SB</span><span class="p">)</span>
</pre></div>

<p>
Once the data is coped, the <tt>main</tt> function is executed. Perhaps this
should really call the main function with <tt>BL</tt> and run an infinite loop
when it returns. In practice all the <tt>main</tt> functions in the test
programs never return because they all end with an infinite loop.
</p>

<h2>Main functions</h2>

<p>
I won't say much about these, other than to mention that the test programs all
use <tt>main</tt> functions with <tt>void main(void)</tt> signatures. This is
because the starting point for this activity was porting Inferno to different
platforms, and the ports all tend to use <tt>main</tt> functions of this type.
There is often nothing that needs to pass arguments to the function, and
nothing to return a result back to.
</p>

<h2>Next steps</h2>

<p>
I'll get back to looking at floating point support for Thumb-2. It's useful to
look at how exceptions are handled for the old ARM ports because something is
going to need to handle double precision floating point numbers if we want to
use them in our code.
</p>
<!-- /content -->

<!-- name -->David Boddie<!-- /name -->
<!-- date -->30 January 2022<!-- /date -->
