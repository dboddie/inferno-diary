implement Test;

include "sys.m";
include "draw.m";
Context, Display, Font, Image, Point, Rect: import draw;

draw: Draw;
display: ref Display;
font: ref Font;

Test: module
{
    init: fn(nil: ref Context, nil: list of string);
};

Label: adt
{
    left, centre, right: con iota;

    text: string;
    font: ref Font;
    alignment: int;

    draw: fn(label: self ref Label, rect: Rect);
};

Label.draw(label: self ref Label, rect: Rect)
{
    x: int;

    case label.alignment {
    Label.left =>
        x = rect.min.x;
    Label.centre =>
        width := label.font.width(label.text);
        x = rect.min.x + (rect.dx() - width) / 2;
    Label.right =>
        width := label.font.width(label.text);
        x = rect.max.x - width;
    * =>
        x = rect.min.x;
    }

    display.image.text((x, rect.min.y), display.white, (0,0), label.font,
                       label.text);
}

init(context: ref Context, nil: list of string)
{
    draw = load Draw Draw->PATH;
    display = Display.allocate(nil);
    font = Font.open(display, "*default*");

    # Draw the background, filling the entire screen.
    display.image.draw(Rect(Point(0,0), Point(320,240)), display.rgb(0,0,64),
                       nil, (0,0));

    # Create labels to show the text with different alignments.
    left_label := ref Label("Left", font, Label.left);
    centre_label := ref Label("Centre", font, Label.centre);
    right_label := ref Label("Right", font, Label.right);

    # Draw the labels.
    left_label.draw(Rect((2,50), (318,90)));
    centre_label.draw(Rect((2,100), (318,140)));
    right_label.draw(Rect((2,150), (318,190)));
}
