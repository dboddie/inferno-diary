implement Test;

include "sys.m";
include "draw.m";
Context, Display, Font, Image, Point, Rect: import draw;

draw: Draw;
display: ref Display;
font: ref Font;

Test: module
{
    init: fn(nil: ref Context, nil: list of string);
};

init(context: ref Context, nil: list of string)
{
    draw = load Draw Draw->PATH;
    display = Display.allocate(nil);
    font = Font.open(display, "*default*");

    # Draw the background, filling the entire screen.
    display.image.draw(Rect(Point(0,0), Point(320,240)), display.rgb(0,0,64),
                       nil, (0,0));
    # Draw the title text.
    display.image.text(Point(2, 2), display.white, (0,0), font, "Hello world!");
}
