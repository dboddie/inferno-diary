implement Test;

include "sys.m";
include "draw.m";
Context, Display, Image, Point, Rect: import draw;

draw: Draw;

Test: module
{
    init: fn(nil: ref Context, nil: list of string);
};

init(context: ref Context, nil: list of string)
{
    # Use the Draw module to draw on the screen.
    draw = load Draw Draw->PATH;
    display := draw->Display.allocate(nil);
    for (b := 0; b < 256; b++) {
        display.image.draw(Rect(Point(b, 0), Point(b + 1, 4)), display.rgb(b, 0, 0), display.opaque, Point(0, 0));
        display.image.draw(Rect(Point(b, 4), Point(b + 1, 8)), display.rgb(0, b, 0), display.opaque, Point(0, 0));
        display.image.draw(Rect(Point(b, 8), Point(b + 1, 12)), display.rgb(0, 0, b), display.opaque, Point(0, 0));
        display.image.draw(Rect(Point(b, 12), Point(b + 1, 16)), display.rgb(b, b, b), display.opaque, Point(0, 0));
    }
}
