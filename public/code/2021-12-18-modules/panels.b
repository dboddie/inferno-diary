implement Panels;

include "sys.m";
include "draw.m";
Display, Font, Image, Rect: import draw;

include "panels.m";

draw: Draw;

Label.draw(label: self ref Label, rect: Rect)
{
    x: int;
    font := label.font;
    if (font == nil)
        font = default_font;

    case label.alignment {
    Label.left =>
        x = rect.min.x;
    Label.centre =>
        width := label.font.width(label.text);
        x = rect.min.x + (rect.dx() - width) / 2;
    Label.right =>
        width := label.font.width(label.text);
        x = rect.max.x - width;
    * =>
        x = rect.min.x;
    }

    display.image.text((x, rect.min.y), display.white, (0,0), label.font,
                       label.text);
}

init(ctx: ref Draw->Context)
{
    draw = load Draw Draw->PATH;
    display = Display.allocate(nil);
    default_font = Font.open(display, "*default*");
}
