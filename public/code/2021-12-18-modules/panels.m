Panels: module
{
    PATH: con "panels.dis";

    default_font: ref Draw->Font;
    display: ref Draw->Display;

    init: fn(ctx: ref Draw->Context);

    Label: adt
    {
        left, centre, right: con iota;

        text: string;
        font: ref Draw->Font;
        alignment: int;

        draw: fn(label: self ref Label, rect: Draw->Rect);
    };
};
