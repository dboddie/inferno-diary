implement Test;

include "sys.m";
include "draw.m";
draw: Draw;
# These imports are needed to allow method calls on these types:
Display, Image, Rect: import draw;

include "panels.m";
panels: Panels;
default_font, display, Label: import panels;

Test: module
{
    init: fn(nil: ref Draw->Context, nil: list of string);
};

init(context: ref Draw->Context, nil: list of string)
{
    panels = load Panels Panels->PATH;
    draw = load Draw Draw->PATH;
    
    # Initialise the panels instance.
    panels->init(context);

    # Draw the background, filling the entire screen.
    display.image.draw(Rect((0,0), (320,240)), display.rgb(0,0,64),
                       nil, (0,0));

    # Create labels to show the text with different alignments.
    left_label := ref Label("Left", default_font, Label.left);
    centre_label := ref Label("Centre", default_font, Label.centre);
    right_label := ref Label("Right", default_font, Label.right);

    # Draw the labels.
    left_label.draw(Rect((2,50), (318,90)));
    centre_label.draw(Rect((2,100), (318,140)));
    right_label.draw(Rect((2,150), (318,190)));
}
