#!/usr/bin/env python3

import datetime, html, os, re, shutil, sys

base_url = "https://dboddie.gitlab.io/inferno-diary"

rss_header = """<?xml version='1.0' encoding='UTF-8' standalone='yes'?>
<rss version="2.0">
  <channel>
    <title>Inferno Diary</title>
    <link>%(url)s</link>
    <description>Articles about things I've been doing with the Inferno operating system.</description>
    <language>en</language>
    <lastBuildDate>%(date)s</lastBuildDate>
"""

item_template = """      <item>
        <title>%(title)s</title>
        <link>%(link)s</link>
        <guid>%(guid)s</guid>
        <pubDate>%(date)s</pubDate>
        <description>%(description)s</description>
        <author>david@boddie.org.uk (David Boddie)</author>
      </item>
"""

rss_footer = """  </channel>
</rss>"""

if __name__ == "__main__":

    if len(sys.argv) != 5:
        sys.stderr.write("Usage: %s <templates directory> <sources directory> "
                         "<output directory> <encoding>\n" % sys.argv[0])
        sys.exit(1)

    encoding = sys.argv[4]
    templates_dir = sys.argv[1]
    sources_dir = sys.argv[2]
    output_dir = sys.argv[3]
    rss_file = os.path.join(output_dir, "updates.rss")

    # Keep a record of the global substitutions.
    global_subs = {"<!-- #entries -->": ["<ul>", "</ul>"]}

    # Keep a record of loaded templates for reuse.
    templates = {}

    names = []
    index = open(os.path.join(sources_dir, "index"), "r", encoding=encoding)
    for line in index.readlines():
        names.append(line.strip())

    rss_feed = []

    r = re.compile("<!-- ([^-]+) -->")

    for name in names:

        text = open(os.path.join(sources_dir, name), "r", encoding=encoding).read()

        # Find all the content to put into the template.
        subs = {}
        i = 0
        while i < len(text):
            # Find a start tag.
            m = r.search(text, i)
            if not m: break
            tag = m.group(0)
            label = m.group(1)
            i = m.end()

            # Find the corresponding end tag.
            m = r.search(text, i)
            if not m: break
            if m.group(1) != "/" + label:
                break

            subs[tag] = text[i:m.start()]
            i = m.end()

        title = subs.get("<!-- title -->")
        if not title: continue
        date = subs.get("<!-- date -->")
        if not date: continue

        if not "<!-- #entries -->" in subs:
            global_subs["<!-- #entries -->"].insert(-2,
                '<li>%s: <a href="%s">%s</a></li>' % (date, name, title))

        summary = subs.get("<!-- summary -->", title)
        rss_feed.append((date, name, title, summary))

        template_name = subs.get("<!-- template -->")
        if not template_name: continue

        if template_name in templates:
            template = templates[template_name]
        else:
            templates[template_name] = template = \
                open(os.path.join(templates_dir, template_name), "r", encoding=encoding).read()

        # Substitute content into the template and write the text to a file.
        output = template
        for s in subs:
            if s in global_subs:
                output = output.replace(s, "\n".join(global_subs[s]))
            else:
                output = output.replace(s, subs[s])

        open(os.path.join(output_dir, name), "w", encoding=encoding).write(output)

    update_time = datetime.datetime.utcnow()
    date_format = "%Y-%m-%d %H:%M:%S UTC"

    f = open(rss_file, "w")
    f.write(rss_header % {
        "date": update_time.strftime(date_format),
        "url": base_url
        })

    for date, name, title, summary in rss_feed:

        day, month, year = date.split()
        month = {"January": 1, "February": 2, "March": 3, "April": 4, "May": 5,
                 "June": 6, "July": 7, "August": 8, "September": 9,
                 "October": 10, "November": 11, "December": 12}[month]

        f.write(item_template % {
            "title": title,
            "link": base_url + "/" + name,
            "guid": base_url + "/" + name,
            "date": "%i-%02i-%02i" % (int(year), month, int(day)),
            "description": html.escape(summary)
            })

    f.write(rss_footer)
